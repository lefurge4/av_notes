# Debug with VSCode

### Author: Daniel Morris
### [Index](../Readme.md)
___
## Contents
* [IDE Installation](#IDE-Installation)
  * [Run](#Run)
* [Debug Preliminaries](#Debug-Preliminaries)
  * [Folders](#Folders)
  * [Workspaces](#Workspaces)
  * [Working Directory](#Working-Directory)
* [Example 1: Python Debugging](#Example-1-Python-Debugging)
* [Example 2: ROS-Python Debugging](#Example-2-ROS-Python-Debugging)

___
# IDE Installation

Installation of VSCode is straight forward.  Here are instructions that depend on what environment you are running.

## EGR VDI Environment
The *EGR VDI* environment has VSCode pre-installed, so no need to install anything.  

For some users the Windows version does not run Python in WSL.  **Only if you have trouble running Python in VSCode**, then install the Linux version.  Do this in a WSL window by executing the following:
```
sudo apt-get update && sudo apt-get upgrade -y
sudo apt install software-properties-common apt-transport-https wget
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" 
export DONT_PROMPT_WSL_INSTALL=1
sudo apt install code
sudo apt-get update && sudo apt-get upgrade -y
```

If you install this and VSCode fails to run, then it could be your `DISPLAY` variable is incorrect.  See the [Ubuntu](Ubuntu.md) notes on how to set this variable.  Also, with Ubuntu VSCode, files that start with a "`.`" will be hidden, making it hard to edit your `.bashrc` file.  To reveal these files: after you select `Open File...` from the `File` menu, right click on a file and check the `Show Hidden Files` box like below:

![Hidden Files](.Images/hidden_files.png)

## Home Windows or Mac
Installation details are at [https://code.visualstudio.com](https://code.visualstudio.com). 

## Home Ubuntu 
If you are running Ubuntu and ROS on a remote computer, details of how to remotely edit files from your home OS are [Setup/Optional/VSCode_Remote.md](Optional/VSCode_Remote.md).  


## Run
On your Ubuntu command line type:
```
code <folder_name>
```
To open the current folder type:
```
code .
```


___
# Debug Preliminaries

VSCode provides powerful debugging capabilities.  You can step through code and examine variable contents.  It is easy to debug ROS nodes.  This document steps through the process of debugging a ROS node.  It also demonstrates how to pass arguments to your code when you run a debug session. But first, a couple preliminaries before you start debugging.  

## Preliminaries 
First set up a Virtual Python environment, as explained in [Setup/Python_Environment.md](Python_Environment.md)

## Folders
In VSCode, rather than opening a single file, it is more usual to open a folder (from the File Menu).  This is typically the source folder for a project.  You can set debug commands for various files within this folder.

## Workspaces
You can also create a workspace, that contains one or more folders, from the File menu.  A workspace can share properties and folders with files can be viewed in the Explorer on th left.  Workspaces particularly useful for remote-SSH sessions.  Adding a folder will require inputting your password (unless you have set up a public/private key).  But once you have added the folder to your workspace, you can open and add files to it without re-entering your password. 

## Working Directory

When running debug, your Python code runs relative to the Folder or Workspace definition.  If you are opening a file in your code, it can be tricky to know which folder one needs to specify it with repect to.  A simple way to find the working folder is as follows:

1. Run your code and put a break at the line you are concerned with.
2. Click on 'DEBUG CONSOLE' in the top ribbon of the OUTPUT window.  
3. Type:
```bash
import os
os.getcwd()
```

___
# Example 1: Python Debugging

This is a quick-start example of debugging Python in VSCode.  See [Example 2](#Example-2-ROS-Python-Debugging) below for a second, more in-depth example that includes a ROS node.

Following the directions in the [VSCode install docs](VSCode_Install.md), open VSCode in your home OS, and begin a remote session in your Ubuntu environment.  This may be via `ssh`, `WSL` or `Docker`.  

*  Open file [python/hello_user.py](python/hello_user.py) in your VSCode editor.  

If you are viewing this document on the Gitlab website or on Windows, you'll need to transfer this file to your Ubuntu instance.  The easiest way to do this is by cloning this repo in Ubuntu.  Alternatively, you could copy it via a mounted drive.  In WSL and Docker you can likely find a mount of your home OS in `/mnt/`. (In some cases copying source code from Windows can be problematic as Windows uses a carriage-return and line-feed at the end of each line, unlike linux see [Trouble_shooting](Trouble_Shooting.md).) 

You should now have a Python file in your VSCode window.  The first few lines of it are shown below.  It is a simple Python class that uses OpenCV to display a rotating message in a window.  Make sure you understand how it works, as you will be implementing similar programming problems.
```python
#!/usr/bin/env python
'''
    hello_user.py 

    This greets the user with a rotating message
'''
import numpy as np
import cv2
import os

class ImageRotator:

    def __init__(self, message=''):
        self.img = []
        self.angle = 0.
        self.create_hello_image(message)

# <Open python/hello_user.py to see rest of code>
```
When you open a Python file you may be presented with a warning about selecting a Python interpreter like this:

![Select Interpreter](.Images/pythonSelectInterpreter.png)

Click on the Yellow highlighted text to get an option like this from which you can select which Python interpreter you want to execute your code:

![Select Interpreter](.Images/pythonSelectInterpreter2.png)

After you select your interpreter, possibly the `work` environment you created above, you should see your interpreter at the bottom of the VSCode window:

![Select Interpreter](.Images/pythonSelectInterpreter3.png)

By clicking on this you can change your interpreter any time you want.


Now, start Debug from the left bar of VSCode:

![Run Debug](.Images/debug.png) 

If you want to pass arguments to your code, you would select `create a launch.json file`, and set up a configuration for your code (see Example 2 below).  In this case you can simply click on `Run and Debug` above, and then select: `Python File`:

![Run Python File](.Images/pythonFile.png)

If you get an error in the `import` lines, then as in Section 1, ensure that the underlay has been sourced in the VSCode terminal using: `source /opt/ros/noetic/setup.bash`.   Sourcing the underlay is sufficient to get the libraries.  If you are debugging a ROS node (as opposed to simply a Python file like this one), then you will want to source the overlay, namely: `source ~/catkin_ws/devel/setup.bash`.   When your code runs you should see an output like this:

![Hello](.Images/hello.png)

To quit, click on the window and press `q`.

___
# Example 2: ROS-Python Debugging

The following example assumes you have assumes you have ROS working and have  created a package for this called `circle` as explained in *Example Node for Driving a Turtlebot with Arguments* near the end of the [../ROS/Turtlebots_Gazebo.md](../ROS/Turtlebots_Gazebo.md) document.

When you run a ROS node, VSCode needs to know where to find the libraries (such as `rospy`) that your code will import.  This is done by sourcing the underlay.  Likely this is done in your `~/.bashrc` file.  If not, you can do this directly in the VSCode terminal with the command:
````
source /opt/ros/noetic/setup.bash
````

Open the `~/catkin_ws/src/circle` folder in VSCode (from the File menu).  Click on the `circle_drive.py` file in the Explorer so that it opens like this:

![Debug](.Images/vscode_debug1.png)

The first time you open a Python file it will prompt you to install an extension, which you should do.

![Python](.Images/vscode_python.png)

It will also prompt you to select an interpreter.  I recommend using the `(work)` virtual environment that we set up in [Python_Environment](Python_Environment.md).  

![Python](.Images/vscode_python2.png)

Installing `pylint` is also useful:

![Python](.Images/vscode_python3.png)

You can see it installed with pip:

![Python](.Images/vscode_python4.png)

Now VSCode has been configured to edit and run Python files.  To debug, click on the left play button (below), but rather than clicking on `Run and Debug`, you can configure your debugging sessions and add arguments by creating a `launch.json` file as below:

![Python](.Images/vscode_python5.png)

This will bring up your new `launch.json` file, which currently specifies an option to execute the current file:

![Python](.Images/vscode_python6.png)

We can configure a debug run for the `circle_drive.py` node as follows.  Copy and paste the following into your `launch.json` file:
```
        {
            "name": "circle_drive",
            "type": "python",
            "request": "launch",
            "program": "${workspaceFolder}/src/circle_drive.py",
            "console": "integratedTerminal",
            "args": ["--time", "6.0",
                     "--linear","0.25",
                     "--angular","0.8",
            ]
        },    
```
![Python](.Images/vscode_python7.jpg)

Notice a few things.  I also inserted a comma after the `Python: Current File` configuration.  I gave it a name to recognize it by.  Anything will do, but I chose `circle_drive`.  In the `"program":` line I added the path to the Python code from the workspace folder (which is the folder you originally opened, in this case `~/catkin_ws/src/circle`).  Finally, I added a set of arguments.  

When you are ready to debug, select the name of your configuration from the dropdown menu as shown:

![Python](.Images/vscode_python8.png)

Then press Play, and your code will run and output text to the terminal window.  Of course you will need `roscore` running in a separate terminal if you are debugging a ROS Node:

![Python](.Images/vscode_python9.png)

Now you can insert break points to stop the code.  You can examine python variables in the `DEBUG CONSOLE` (located next to `TERMINAL`).

___
### [Back to Index](../Readme.md)
