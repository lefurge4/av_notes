# Assignments and Git

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
### [Index](../Readme.md)
## Contents
* [Git Basics](#Git-Basics)
* [Lab Assignments](#Lab-Assignments)
___
# Git Basics

Git is an important tool for this course.  You will be using it to download class notes and assignments, as well as to upload your completed labs to the MST Gitlab server.  It is a powerful source management tool with extensive documentation available.  However, for this class, only a basic knowledge of it is necessary.  This document will get you started with Git if you are not familiar with it.

First, I recommend reading the [Gitlab basics](https://gitlab.msu.edu/help/gitlab-basics/start-using-git.md) documentation on Gitlab.

There are a few things to be aware of for this course:

* I recommend cloning with the `https` option rather than `ssh`.  This is because MSU blocks ssh access to most computers (except a few gateways) from outside the university.  
* If you wish to use multiple branches you may.  However, when you submit your assignments it must always be merged to the `master` branch as this is what the grader will pull.  The easiest thing if you are not familiar with branches is only to use the `master` branch.  

## Git in VSCode
It turns out that VSCode integrates very nicely with Git.  You can commit, push, pull all from the VSCode interface without needing to use a terminal.  Once you figure out how to use this interface, you won't want to use Git any other way.  Instructions are here: [https://code.visualstudio.com/docs/editor/versioncontrol](https://code.visualstudio.com/docs/editor/versioncontrol).


___
# Lab Assignments

Instructions on obtaining and submitting lab assignments are here: [https://gitlab.msu.edu/labs/labs_2021](https://gitlab.msu.edu/labs/labs_2021)

You must be enrolled in the class to access this folder.

## Credentials

One downside of using https authentication is that every time you pull or push you need to input your credentials.  If you execute the following command within a repo, it will remember your credentials for that repo for an extended time, avoiding the need to enter your username and password each time:
```
git config credential.helper 'cache --timeout=100000'
```

___
### [Back to Index](../Readme.md)
