#  noetic_full.Dockerfile
#
#  Docker for a customized Noetic ROS instance
#  Includes the full desktop (with Gazebo)
#  Creates a local user called avc
#  Mounts a folders from the host machine to /mnt/code and /mnt/data, and links these to the home folder
#    The idea is to store all src files on the local machine so they are not lost when the docker is removed
#    You can create a sumbolic link to them from within a catkin_ws
# 
#  Daniel Morris, Sep 2020
#
# Here are various Docker commands I used to create the Docker image, and to run it:
#  docker login
#  docker build -t morris2001/noetic_full -f noetic_full.Dockerfile .
#  docker push morris2001/noetic_full
# To run it, give it whatever name you like.  I chose noetic1:
#  docker run -it -v /home/dmorris/repos:/mnt/code -v /home/dmorris/data:/mnt/data --name noetic1 morris2001/noetic_full
# (A Windows docker would start the path with c:/Users/dmorris/..., but this is less efficient than using WSL.)
#
# To open an additional shell in this docker container when it is running:
#  docker exec -it noetic1 bash
# When you exit all your noetic1 containers, it will stop.  You can see all your containers and their
# status with the command:
#  docker ps -a
# To start it do:
#  docker start noetic1
# And then use the above "docker exec ..." command to open a shell in this container.  It should have
# kept any changes to your filesytem.
# If you get a new noetic_full image and want to run it as noetic1, then you'll need to delete
# the current noetic1 container with:
#  docker stop noetic1
#  docker rm noetic1
# Note: this will delete all the changes you made to the filesystem.  That is why we keep our code and data 
# on the host filesystem and just create symbolic links to them.
FROM osrf/ros:noetic-desktop-full

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

ENV ROS_DISTRO noetic

# Key development tools:
RUN apt-get update && \
    apt-get install -y python3-pip python3-tk git && \
    rm -rf /bar/lib/apt/lists/*

# ROS Noetic tools:
RUN apt-get update && \
    apt-get install -y ros-noetic-joy ros-noetic-teleop-twist-joy \
      ros-noetic-teleop-twist-keyboard ros-noetic-laser-proc \
      ros-noetic-rgbd-launch ros-noetic-rosserial-arduino \
      ros-noetic-rosserial-python ros-noetic-rosserial-client \
      ros-noetic-rosserial-msgs ros-noetic-amcl ros-noetic-map-server \
      ros-noetic-move-base ros-noetic-urdf ros-noetic-xacro \
      ros-noetic-compressed-image-transport ros-noetic-rqt-image-view \
      ros-noetic-gmapping ros-noetic-navigation ros-noetic-interactive-markers && \
    rm -rf /bar/lib/apt/lists/*

# Turtlebot Packages:
RUN /bin/bash -c '. /opt/ros/${ROS_DISTRO}/setup.bash' && \
    apt-get update && \
    apt-get install -y ros-noetic-turtlebot3-msgs ros-noetic-turtlebot3 && \
    rm -rf /bar/lib/apt/lists/*

# Add basic user
ARG USERNAME=avc
ARG USERID=1001

ENV USERNAME ${USERNAME}
ENV USERID ${USERID}

RUN useradd -m $USERNAME && \
        echo "$USERNAME:$USERNAME" | chpasswd && \
        usermod --shell /bin/bash $USERNAME && \
        usermod -aG dialout,sudo $USERNAME && \
        echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers && \
        chmod 0440 /etc/sudoers && \
        usermod  --uid $USERID $USERNAME

# Change user
USER $USERNAME

# Since this docker only ever runs ROS Noetic, let's source the underlay in the .bashrc file:
RUN echo '. /opt/ros/${ROS_DISTRO}/setup.bash' >> $HOME/.bashrc

# Initialize ROS.  In is not strictly necessary to source the underlay here, but keep it
# in case we decide to remove its addition to the .bashrc file
RUN /bin/bash -c '. /opt/ros/${ROS_DISTRO}/setup.bash; rosdep update' 

#Set up virtual environment:
RUN pip3 install --no-input virtualenv && \
    echo 'export PATH="$PATH:$HOME/.local/bin"' >> $HOME/.bashrc  && \
    mkdir -p $HOME/envs && $HOME/.local/bin/virtualenv --system-site-packages $HOME/envs/work && \
    echo 'alias actwork="source $HOME/envs/work/bin/activate"' 

