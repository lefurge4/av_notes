# Installing an X-Server on Windows and Mac

### Author: Daniel Morris
### [Index](Readme.md)
___

Ubuntu is a flavor of Linux, which uses X11 as a standard protocol for displaying graphics on a monitor.  X11 is nice in that it is simple to select any computer monitor on the network to output graphics.    What is needed is that an X-server be running on the display computer, and the connection be authenticated by the X-server. 

Neither Windows nor Mac support X11, and so we software that runs an X-Server on them.  There are various free and paid options available.  Here is a free option for Windows and Mac

**Mac**: [XQuartz](https://www.xquartz.org/) provides an X-server, and good  online [instructions](https://techsparx.com/software-development/docker/display-x11-apps.html) for how to use it are available.

**Windows**: Download and install: [VcxSrv](https://sourceforge.net/projects/vcxsrv/)

The rest of this document steps through how to use [VcxSrv](https://sourceforge.net/projects/vcxsrv/) as an X-server on a Windows machine.

## Run the VcxSrv X-server
Once VcxSrv is installed, you can launch it typing `xlaunch` into the windows search bar: ![xlaunch](.Images/xlaunch.png).  

This will provide options for display settings.  Select the Multiple windows option: ![Choose Multiple Windows](.Images/xlaunchSelect.png)

![Next select "No Client"](.Images/xlaunchClient.png)

To avoid authentication problems, turn off Native opengl, and select Disable access control:
![Settings](.Images/xlaunchSettings.png)

![Finish](.Images/xlaunchFinish.png)

![Running](.Images/xlaunchResult.png)

## Output to the X-Server

Next find the IP address of the **host** computer (Windows or Mac) that has your monitor.  In the Windows command shell type:
````
ipconfig
````
![IP Address](.Images/IPaddress.png)

This shows my host having an IP address of `192.168.86.33`.  Yours will have a different IP address.  Now within your Ubuntu shell, you can set the `DISPLAY` environment variable to this host computer as follows:
````
export DISPLAY=192.168.86.33:0
````
Or course you would insert your own IP address.  The `:0` sets the monitor number, which is usually 0.  Note: ensure that there are **no spaces** around the `=` sign when using an `export` command.  

If you want to test the ability of your Ubuntu instance to display graphics, you can try the following command:
````
xeyes
````
If this command gives you an error like: `bash: xeyes: command not found`, then install the following app:
````
sudo apt update
sudo apt install x11-apps
````
Provide your password if requested after the first sudo command.  Press Enter when asked to confirm the installation.  After this completes (which may take a while), try the `xeyes` command again.  You should see this if you can display graphics: 

![Xeyes](.Images/xeyes.png)
___
### [Back to Index](Readme.md)
