# ROS Installation

### Author: Daniel Morris
### [Index](Readme.md)
___

* [Install ROS 1 Noetic](#Install-ROS-1-Noetic)
* [Install Additional Packages](#Install-Additional-Packages)
* [Install Turtlebot Packages](#Install-Turtlebot-Packages)
* [Add to your `~/.bashrc` file](#Add-to-your-bashrc-file)
* [Uninstalling ROS and Gazebo](#Uninstalling-ROS-and-Gazebo)

# Install ROS 1 Noetic

Note: If you are using a ROS Docker image, (as described in the [Docker install document](Docker.md)), then you can simply use `docker run ...` it to create a container with ROS pre-installed, and there is no need to install ROS yourself.  All the packages are installed on the provided Noetic Docker Image, only the Turtlebot Simulations need to be installed, see below in [Install Turtlebot Packages](#Install-Turtlebot-Packages).

If you are starting with a running Ubuntu instance, then it is quite simple to install ROS Noetic.  Follow the instructions on this page: 
[wiki.ros.org/noetic/Installation/Ubuntu](http://wiki.ros.org/noetic/Installation/Ubuntu).  The download may take a while, and you can do other work while it installs.  Note: In section 1.4, install the `Desktop-Full` in order to get all the packages you'll need; otherwise you will need to install various ROS packages along the way.

You will find that ROS is installed into the folder `/opt/ros/<distro>`, where `<distro>` is `noetic`.  Other ROS versions are installed in their own subfolder here.  For example, the recent ROS 2 release, Foxy, would be installed in `/opt/ros/foxy`.  

# Install Additional Packages

To install additional ROS packages that do not come with the ROS install use the command:
```
sudo apt install ros-noetic-<package-name>
```
We will use a number of additional packages which you should install as follows:
```
sudo apt update
sudo apt install ros-noetic-joy ros-noetic-teleop-twist-joy \
  ros-noetic-teleop-twist-keyboard ros-noetic-laser-proc \
  ros-noetic-rgbd-launch ros-noetic-rosserial-arduino \
  ros-noetic-rosserial-python ros-noetic-rosserial-client \
  ros-noetic-rosserial-msgs ros-noetic-amcl ros-noetic-map-server \
  ros-noetic-move-base ros-noetic-urdf ros-noetic-xacro \
  ros-noetic-compressed-image-transport ros-noetic-rqt-image-view \
  ros-noetic-gmapping ros-noetic-navigation ros-noetic-interactive-markers
```

# Install Turtlebot Packages

Install pre-built Turtlebot packages with these commands:
```
sudo apt update
sudo apt install ros-noetic-turtlebot3-msgs ros-noetic-turtlebot3
```

And then install from source the Turtlebot Simulations by building it as follows:
```
source /opt/ros/noetic/setup.bash
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
git clone -b noetic-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
cd ~/catkin_ws && catkin_make
source devel/setup.bash
```
Make sure the build completes successfully.  If you get a python build error, you may need to install your virtual Python environment as explained in [Python_Environment.md](Python_Environment.md).  Activate it, and then rerun the last two lines of the above.

# Add to your `~/.bashrc` file

Depending on your Ubuntu environment, there are different recommended configurations to add to your `~/.bashrc` file (which gets executed each time you open or new shell, or manually type: `source ~/.bashrc`).  You can edit this file using VSCode by typing `code` into the command line.

For all cases I recommend adding:
```
export TURTLEBOT3_MODEL=burger                  # For running Turtlebots
```

If you are using the pre-configured Engineering VDI remote desktop with WSL, the following should already be in it:
```
export LIBGL_ALWAYS_INDIRECT=0
export DISPLAY=$(ip route | awk '/^default/{print $3; exit}'):0
```

If you are using WSL or Docker at home, then find the IP address of your main OS and insert it below `<Base_OS_IP_Address>` when you add this to your `~/.bashrc` file:
```
export LIBGL_ALWAYS_INDIRECT=0                  # Only for WSL and Docker
export DISPLAY=<Base_OS_IP_Address>:0           # Only for WSL and Docker -- insert your Windows IP address here, and no spaces
```

___
# Uninstalling ROS and Gazebo

It is always good to know how to unintall ROS.  You can do it with:

## Uninstall ROS
To uninstall all ROS versions:
```
sudo apt-get remove ros-*
```
To uninstall just ROS Noetic:
```
sudo apt-get remove ros-noetic*
```
Then do:
```
sudo apt-get autoremove
```
## Uninstall Gazebo:

Gazebo is a separate simulator

```
sudo apt-get remove ros-noetic-gazebo*
sudo apt-get remove libgazebo*
sudo apt-get remove gazebo*
```
___
### [Back to Index](Readme.md)
