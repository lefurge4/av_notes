# Optional Resources

This folder contains how-to instructions for a number of optional PC configurations, particularly configurations you can install at your home.  You can attempt these on your own -- they will not be supported by the course.  However, it can be nice to have ROS running on your own PC without needing to connect to the VDI remote desktop, and these can help you get going. 

* [Install Ubuntu at Home](Ubuntu_home.md)  
  * How to get Ubuntu on your home PC
* [Set up an X-Server](X_Server.md)  
  * This is needed for displaying graphical data from within WSL
* [Remote Editing with VSCode](VSCode_Remote.md)
  * Edit code in a Docker or WSL or other remote platform from within your home OS.
* [Using Carla](Carla.md)
  * Carla is a powerful simulator, although using it with ROS is a bit tricky
* [PyCharm](PyCharm.md)
  * PyCharm is an alternate IDE to VSCode, although I don't recommend it
* [Install ROS](ROS.md)
  * Install ROS in your home Ubuntu environment

