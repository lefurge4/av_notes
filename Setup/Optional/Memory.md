# PC Memory Usage

### Author: Daniel Morris

### [Index](Readme.md)
___

# Memory Usage

Running WSL, Virtual Machines, Docker etc. can be memory intensive.  If you have 16 GB or less memory, it is a good idea to monitor your memory usage, which you can do with the Task Manager in Windows.  The `Vmmem` task appears to be the primary resource used by these processes.  The following are the memory footprints in my 16 GB home PC as I start various tasks:

First, starting Ubuntu 20.04 in WSL I obtain:

![Vmmem](.Images/Vmmem2.png)

Next, opening VSCode in WSL configuration:

![Vmmem](.Images/Vmmem3.png)

Next, starting Docker:

![Vmmem](.Images/Vmmem4.png)

Running both `melodic1` and `noetic1` in Docker:

![Vmmem](.Images/Vmmem5.png)

After 6 or so hours later without using them, the memory use increases, see below.  I'm concerned that there is a slow memory leak, (although there may be more benign explanations).  If memory usage grows too much, it may be necessary to occasionally restart the VM processes.  The safest way I know of doing this is to sign out and sign back in. 

![Vmmem](.Images/Vmmem7.png)



___
### [Back to Index](Readme.md)
