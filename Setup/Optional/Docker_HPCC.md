# Docker on HPCC

This document describes how to create and use a Singularity container from a Docker image on HPCC.  A Singularity container is similar to the Docker container you would get from the Docker image.  All the ROS commands you learn can be applied in it. However, some key differences are:
 * You will not have sudo privledge in the container
 * The container overlays on your HPCC user account

## Why HPCC?

Running Docker on your home PC will demand good bit of processing and memory.  If you have under 16 GB RAM, running Gazebo and other processes in Docker may be too much.  In this case you can run Docker on HPCC.  And it is not an either/or.  You can do some tasks on your home PC and some on HPCC using Git to transfer your code.  I recommend that initially you start with a Docker on your home PC, and if this become too slow or uses too much memory, then use HPCC.

If you already have an account on HPCC, you can use that.  Otherwise, you will need to send a message to your instructor on Piazza **providing your NetID and saying you want to run Docker on HPCC**, and he will request an account for you.  Likely it will take a few days, but could take up to a week. You should receive an automated messge from HPCC with instructions for logging in.

## HPCC Info
There is good documentation on the HPCC including:
* [Main documentation page](https://wiki.hpcc.msu.edu/).  Please consult and search this for questions first.
* [Web access to HPCC](https://webrdp.hpcc.msu.edu) or other [ways including ssh](https://wiki.hpcc.msu.edu/display/ITH/How+to+Access+HPCC)
* [Submit a help request](https://contact.icer.msu.edu/contact).  The ICER staff are very helpful and usually respond within a few hours or a day.

### Using a Development Node
When you [connect to HPCC](https://wiki.hpcc.msu.edu/display/ITH/Web+Site+Access+to+HPCC) and start a terminal, it will be one of the *gateway* nodes.  You can see your HPCC home folder here, but do **not** run processes on gateway nodes.  Rather you should *ssh* to a [development](https://wiki.hpcc.msu.edu/display/ITH/Development+nodes) node and do your work there.  These nodes are meant for development (not long jobs) and give you two hours of CPU time before they log you off.  For this class that will be sufficient -- if they log you off, simply reconnect.  When you first connect you'll see a list of available nodes and their current usage (code colored) -- select whichever node has the lowest current usage.  The figure below shows an example of my connecting to a gateway (in this case *rdpgw-01*) and then connecting via ssh to a development node (in this case *dev-intel14*).

![Connected to a gateway, followed by ssh to development node dev-intel14](.Images/IcerLogin.jpg)

Which nodes are low-use will vary by day and time.  As you can see, I typed the following to connect to a currently low-use development node:
```
ssh dev-intel14
```

## ROS under a Singularity on HPCC
HPCC development nodes uses *CentOS*, not *Ubuntu*.  So in order to run ROS you will need a virtual *Ubuntu* instance.  I created a docker instance of an Ubuntu 18.04 machine, pre-installed with ROS Melodic, the Turtlebot simulations, and some sample code from our labs.  To install it you will use a Singularity (https://wiki.hpcc.msu.edu/display/ITH/Singularity%3A+I.+Introduction) which can import Docker images.  The following are instructions for installing our ROS environment using Singularity.  

In your development node under *CentOS* pull our Docker image and give it the name `noetic1` as follows:
```
singularity pull --name noetic1 docker://morris2001/noetic_full
```
This will download the image into a read-only container called `noetic1`.  When done, you can open a shell in this container using:
```
singularity shell noetic1
```
![Starting Ubuntu Container](.Images/Centos_Ubuntu.png)

Notice that in this container your user ID remains the same as your HPCC ID and you remain in your HPCC home folder, even though the OS is *Ubuntu* rather than *CentOS*.  Actually, all your HPCC files are visible and readable/writable from the Singularity container.  In addition, a `/opt/ros/noetic/` folder with an install of ROS is visible.  It is just not possible to update this install using `sudo apt-get install` as there is not root privledge in this container.

When you create your workspace, as described in [ROS_Start](../ROS/ROS_Start.md), you can create the folder either inside the singularity or inside *CentOS*; it makes no difference as the same `~/catkin_ws/src` folder will be created.  However, there are two aspects that differ:
 * All ROS commands need to be run inside the Singularity Container.  This includes `catkin_create_pkg`.  You'll treat the container just like the other Docker containers.
 * File editing is better done from the *CentOS*.  The preferred editor for Python is VSCode which you can run from your home PC as explained in [VSCode_Editing](VSCode_Editing.md).


## Preparing for Running ROS on HPCC

There may be multiple users running ROS on each HPCC development node.  This creates a problem as the `roscore` and `gazebo_ros` processes from different users will interfere with each other.  The way to avoid interference is for each user to use unique ports for both `roscore` and `gazebo_ros`.  The following setup will enable you to obtain unique ports for your ROS and Gazebo processes.

Note that another complication is that your `.bashrc` file must handle double duty: it will be run whenever either a *CentOS* or *Ubuntu* shell starts.  

Use your favorite editor in your *CentOS* instance (such as `gedit`) to paste the following lines to the end of your `.bashrc` file:
```
# Ports are available in this range: 49152 to 65535
export ROS_PORT=51323       # Choose your own value from the above range (no spaces around the '=')
export GAZEBO_PORT=51324    # Choose a different value from the above range (no spaces around the '=')
alias portsInUse='netstat -plnt |grep -e $ROS_PORT -e $GAZEBO_PORT'
alias rosgo='roscore -p $ROS_PORT'

if [ -d "/opt/ros" ]; then
    #We are in a ROS singularity, so call setup:
    export ROS_WS="/mnt/home/$USER/catkin_ws"
    echo "ROS_WS: $ROS_WS"
    source /opt/ros/melodic/setup.bash
    source $ROS_WS/devel/setup.sh
    export TURTLEBOT3_MODEL=burger
    export ROS_MASTER_URI=http://localhost:$ROS_PORT
    export GAZEBO_MASTER_URI=http://localhost:$GAZEBO_PORT
fi
```
After you paste the above into your `.bashrc` file, then make two changes: choose different numbers for `ROS_PORT` and `GAZEBO_PORT`.  The above are the numbers I chose, and if you use the same ones, your `roscore` and `gazebo_ros` processes will conflict with mine.  After you first log in (or do a `source ~/.bashrc`), you can check to see if anyone else is using the ports you chose with the `portsInUse` alias, like this:
```
$ portsInUse
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
```
If you get the above output your are good to go: no one else is using either your ROS or Gazebo ports.  However, if you get a response like this:
```
$ portsInUse
(Not all processes could be identified, non-owned process info
 will not be shown, you would have to be root to see it all.)
tcp        0      0 0.0.0.0:51323           0.0.0.0:*               LISTEN      10689/python  
```
that means someone (could be yourself) is already using port `51323`.  Of course once you have started your `roscore`, you'll get this result as then your port will be in use, so you only need to run this test **before** you start `roscore`.

Now to start `roscore` using your selected port you need the option `-p $ROS_PORT`.  If you notice in the above code for your `.bashrc` I made an alias `rosgo` that will do this.  So from now on, to start `roscore` with your selected port you should use the command:
```
rosgo
```
If you don't, you are likely to have problems running your nodes.

## ROS Demo
We are ready to start using ROS.  Return to your *Ubuntu* instance using the command:
```
singularity run docker://morris2001/ros-tbot:1.0
```
In addition to the Turtlebot packages, you'll find that this repo has been installed as a package called `code491` in `catkin_ws/src`.  It has various python ROS nodes from the course including `waypoints_follower.py`, which is easy to demo in a Gazebo world.  We will show this demo next.

First make sure you have at least 3 terminals running your *Ubuntu instance*.  One way is to start more Mate Terminals and in each ssh into the same development node, and start the singularity.  An easier way uses the Ubuntu instance you just started to create 3 xterm windows by calling this command three times:
```
xterm &
```
In one *Ubuntu* terminal type:
```
roscore
```
This is not strictly necessary, as the Turtlebot code will start roscore in the background on its own.  However, it is a good idea to start it yourself because roscore can end up running in the background and you will not have permissions to kill it on HPCC.  If that happens, use the *System* menu at the top of the web view to shut down your environment, and then reconnect.  With roscore in its own terminal you can always kill it with ctrl-C.

One limit of the development nodes is that they do not permit tasks to take more than 2 hours of CPU time and will kill them if they exceed that.  Most tasks don't use continuous CPU time, but Gazebo does, so this means that Gazebo will be killed after 2 hours of running.  When / if that happens, simply kill the any Gazebo-related nodes and restart the below commands.

![Gabezo close to exceeding 2 hours on](.Images/tasksOnHPCC.jpg)

Next in another *Ubuntu* terminal:
```
roslaunch code491 get_turtle_ready.launch
```
This instantiates a Gazebo world, starts Gmapping-based SLAM, starts `rviz`, starts `move_base`, and publishes a set of 4 waypoints to the `waypoints` topic.  Then to start following waypoints, in another *Ubuntu* terminal type:
```
rosrun code491 waypoints_follow.py
```
If this exits with an error: `Action Server not available!`, then just re-run the command.  Here's an image of the `rviz` view of the Turtlebot as it is reaching waypoint 1.

![Running waypoints_follow.py](.Images/waypoints.jpg)

You can create your own nodes in this *Ubuntu instance*.  It's a good idea to try this.  The Gazebo world for the final project will be posted soon for you to start working on.

# ROS on your home PC with Docker
Now if you like, you can user Docker to run ROS on your home PC or laptop.  Here's a page that gives details: https://jack-kawell.com/2019/09/11/setting-up-ros-in-windows-through-docker/
If you wish to run the Docker instance that is described above, you can use this command:
```
docker run -it morris2001/ros-tbot:1.0
```
Then, after setting the DISPLAY varianble in your *Ubuntu instance* and installing an X-server like [VcXrv](https://sourceforge.net/projects/vcxsrv/) on your PC, you should be able to run the waypoints follower demo just like above.  This is purely optional, so do it for your own enjoyment.  I recommend using HPCC which runs faster for me.  
