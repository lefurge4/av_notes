# Install Ubuntu at Home

### Author: Daniel Morris
### [Index](Readme.md)

* [Home PC Recommendations](#Home-PC-Recommendations)
* [Install an Ubuntu Environment](#Install-an-Ubuntu-Environment)
  * Stand-alone Ubuntu Machine
  * Windows with WSL 2
  * Docker
  * Vitual Machine
* [WSL Notes](#WSL-Notes)
___
# Home PC Recommendations

This is a brief guide for setting up an Ubuntu environment on your PC.  If you are going to install a local copy of Ubuntu, rather than use the **Virtual Desktop available to you**, then the following are recommended hardware you will need.  You will need an x64 Intel/AMD computer with:

| Base OS         | Minimum Memory | Recommended Memory |
| ---             | ---            | ----               |
| Ubuntu          | 4 GB           | 8 GB               |
| Windows 10      | 8 GB           | 16 GB              |
| Mac OS >=10.14  | 8 GB           | 16 GB              |

It is a good idea to monitor your memory usage with whatever Ubuntu solution you choose, see [Memory](Memory.md).  If you do not have the recommended memory, then it is best to select the EGR Virtual Desktop with WSL solution.


___
# Install an Ubuntu Environment

ROS 1 has been developed for Ubuntu.  While there are experimental versions that run on other OSes, those are less mature and harder to install and use.  For this course, you will need an instance of Ubuntu 20.04 LTS.  To get this you have a number of options:

* Find a **stand-alone Ubuntu machine** or create a second partition on your computer in which you install Ubuntu and have access to it via dual booting.  This will likely be your highest performing option and you can do everything directly on your Ubuntu computer.  However, I do not the recommended this option for this course unless you already have a linux computer, as installing Ubuntu can be difficult on some machines due to driver issues.

* Windows with **WSL 2** (Windows Subsystem for Linux) on Windows 10 enables you to run Ubuntu 20.04 natively in Windows.  This is the preferred and easiest *home* solution for Windows users.  [Oneline installation instructions](https://docs.microsoft.com/en-us/windows/wsl/install-win10) are available and simple to follow.  You will also need to install an [X-Server](X_Server.md).  If you are memory constrained, it is preferable to use the **EGR VDI** environment which has WSL 2 pre-installed.

* **Docker** is a containerization tool which you can use to build an environment (including ROS).  It runs on many different hosts regardless of their OS or configuration with less overlead than a VM like VirtualBox.  It is especially useful for deploying and running ROS applications as their are pre-build ROS Docker images that are maintained and can be easily augmented.   A Docker container can be customized to be used as a ROS development environment, see [my instructions on using a Docker container](Docker.md).  The main downside I have with it is that it uses significant memory.

* It is possible to use a **virtual machine** (VM) on your Windows or Mac computer to host an Ubuntu instance. This a versatile solution, and I created [installation instructions for VirtualBox](VirtualBox.md).  However, like Docker, this uses significant memory resources and runs slowly, especially for Gazebo, so I do **not** recommend this solution.

Choose one of these options to get you going with an Ubuntu 20.04 LTS instance.  

___
# WSL Notes

For those using WSL, ensure you have WSL 2, which you can check with the command 
```
wsl.exe -l -v
```
Details on upgrading to WSL 2 from WSL 1 are [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10)

The following make it easy to copy files between Windows and Ubuntu in WSL:
* From an Ubuntu shell you can access Windows files in this folder: `/mnt/c/...`
* From Windows File explorer, you can access your Ubuntu files here: `\\wsl$\Ubuntu-20.04\home` 

I also recommend installing and using [Windows Terminal](https://www.microsoft.com/en-us/p/windows-terminal) as this gives you a tabbed interface for shells. ROS will need many shells running at the same time, so this is quite useful (see image below).

![Windows Terminal](.Images/WindowsTerminal.png)



___
### [Back to Index](Readme.md)
