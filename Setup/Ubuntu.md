# Setup Ubuntu

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
### [Index](../Readme.md)

* [EGR Virtual Desktop with WSL](#EGR-Virtual-Desktop-with-WSL)
* [Update Your `.bashrc` file](#Update-Your-bashrc-file)
* [Ubuntu Commands](#Ubuntu-Commands)
* [Home Folder](#Home-Folder)

ROS 1 has been developed for Ubuntu.  While there are experimental versions that run on other OSes, those are less mature and harder to install and use.  For this course, you will need an instance of Ubuntu 20.04 LTS.  The most straight forward option is to use the EGR Virtual Desktop with WSL, that is configured and provided for you.  This document describes this option.  If you wish to install Ubuntu and ROS on your home PC, it is not too hard and I have included instructions for multiple options here: [av_notes/Setup/Optional/Readme.md](Optional/Readme.md), but try this on your own as they won't be supported by the TA or instructor.

___
# EGR Virtual Desktop with WSL

DECS has created a Windows Virtual Desktop environment that has most of the required software pre-installed including: WSL 2, X-Server, ROS, VSCode and a Python virtual environment.  Your Ubuntu machine disk storage will be maintained between logins.  This will be your quickest solution, and will have minimial memory usage for your home computer.  General instructions for connecting to a VDI server are here: [https://www.egr.msu.edu/decs/help-support/how-to/connect-decs-remote-desktop-services-rds-servers](https://www.egr.msu.edu/decs/help-support/how-to/connect-decs-remote-desktop-services-rds-servers). Select the **VMware Horizon Client Download** and install it.  By the first day of class, when you run this you should see a *ROS VM* VDI remote desktop.  

<p align="middle">
<img src=".Images/VDI.png" width="150">
<img src=".Images/VDI_ROS.png" width="250">
</p>


This is a pre-configured Windows environment with WSL and VS Code pre-installed.  To start a Windows Terminal, type `Windows Terminal` on the Windows icon, and then open `Ubuntu` tabs from the drop-down menu.  

![Windows Terminal](.Images/WindowsTerminal.png)


The following make it easy to copy files between Windows and Ubuntu in WSL:
* From an Ubuntu shell you can access Windows files in this folder: `/mnt/c/...`
* From Windows File explorer, you can access your Ubuntu files here: `\\wsl$\Ubuntu-20.04\home` 

___
# Update Your Debian Package Keys for ROS

The ROS Debian package keys in the VM are old and [need to be updated](https://discourse.ros.org/t/ros-gpg-key-expiration-incident/20669).  You'll see some warnings to this effect when you do this command
```bash
sudo apt update
```
To update the keys do this command:
```bash
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
```
Then do the following command and confirm there are no warnings or errors:
```bash
sudo apt update
```
And after this upgrade old packages with:
```
sudo apt upgrade
```
This may take 5 to 10 minutes and will bring your ROS Noetic install up to date.  Open another tab to continue your setup while this is going on.
___
# Update Your `.bashrc` file

A variety of definitions, aliases and helper functions will ease your use of ROS in Ubuntu.  If you copy these to the **end** of your `~/.bashrc` file, then they will be set each time you open a shell.  Here is the full list, which will be explained in more detail below
```
alias rosunder="source /opt/ros/noetic/setup.bash"          # Source the underlay
alias rosover="source $HOME/catkin_ws/devel/setup.bash"     # Source the overlay
alias sbash='source $HOME/.bashrc'                          # Source the ~/.bashrc file
alias gitcache="git config credential.helper 'cache --timeout=100000'"  # Cache git repo credentials
act() {
    source $HOME/envs/$1/bin/activate       # activate virtual environment in ~/envs/
}
export TURTLEBOT3_MODEL=burger      # For running Turtlebots
rosunder                            # sourcing ROS underlay in .bashrc is convenient, but causes conflicts if
                                    # using additional distros like ROS 2, so remove it in that case  
cd $HOME                            # Start in your Ubunto home folder
```
In addition, for those running WSL you will need to add the following 3 lines to your `.bashrc`:
```
export LIBGL_ALWAYS_INDIRECT=0                  # For WSL
export DONT_PROMPT_WSL_INSTALL=1
export DISPLAY=<Windows-IP4-Address>:0          # Find <Windows-IP4-Address> from PowerShell
```
**But note**: replace the text `<Windows-IP4-Address>` with the IP address of the Windows machine (or VDI Windows machine if you are using Remote Desktop).  You can find this from your PowerShell with the command `ipconfig` like this:

![ipconfig](.Images/ipconfig.png)

In this case the IP address of the Windows machine is: `35.12.216.51`, and this is what replaces `<Windows-IP4-Address>` in the above line.  

After you have added this to your `~/.bashrc` file, then source it with:
```
source ~/.bashrc
```
Or simply start a new shell, as this will be sourced for each new shell.

Then you can check to see if your `DISPLAY` variable is correct with the following command:
```bash
xeyes
```
This should produce the below.  If not then check your `DISPLAY` variable and make sure your `.bashrc` file is sourced.

![xeyes](.Images/xeyes.png)


Now here's an explanation for what some of these commands enable you to do.  The `alias` lines set up short commands that call longer expressions.  So calling:
```
rosunder
```
is an easy way to source the underlay instead of typing `source /opt/ros/noetic/setup.bash`.  Similarly
```
rosover
```
will source the overlay.  Both of these commands you are likely to use a lot.  

Notice that later in the `.bashrc` file the underlay is sourced.  This is just for convenience so that each time you open a new terminal you don't need to remember to source it.  It is fine for our ROS use in this course, but this may cause troubles if you use multiple ROS distros, including newer ROS 2 distros.  In that case you may want to remove this line, and manually call `rosunder` in each shell you use ROS Noetic.

Now I used to create an alias to activate my virtual environment.  But when I started adding multiple virtual environments, this got messy.  It is cleaner to specify an activate command followed by the name of the environment, which is done in the above commands.  Assuming you put your virtual environments in your `$HOME/envs` folder, then if you create a virtual environment called `work`, you can easily activate it by typing:
```
act work
```
If you create another virtual environment in the `~/envs` folder called `foo`, you can activate it simply with `act foo`.  


___
# Ubuntu Commands

Most of the work we will do in Ubuntu will be in a terminal window rather than via a graphical user interface.  The following is a table of Linux commands that I use the most.

| Linux command | Description |
| ------------- | ----------- |
| `cd /path/to/directory` | Change to specified directory |
| `cd` and `cd ~` and `cd /home/$USER` | Change to your Ubuntu home folder  |
| `pwd` | Print working directory |
| `ls`  | List files in working directory |
| `ls -lhrt` | List files sorted in reverse time and show attributes |
| `ln -s <source_path> <target>` | Create a symbolic link to source and put it in target |
| `echo $<ENV_NAME>` | Displays value of a bash environment variable | 
| `printenv` | List all the environment variables |
| `printenv` &#124; `grep ROS` | Ditto, but show only lines that include `ROS` in them.  Useful for confirming ROS underlay and overlay, and settings. |
| `export <ENV_NAME>=<value>` | Set a bash environment variable.  NOTE: make sure there are **no** spaces around the `"="` | 
| `htop` | Shows currently running processes with memory and CPU usage (use `q` to exit) |
| `ctrl-c` | Keyboard interrupt.  Most ROS apps exit gracefully with this interrupt |
| `gedit <filename>` | Edit text files.  Note it is usually better to use VSCode for Python |
| `sudo apt update` | This refreshes the app repository lists.  Always do this before `sudo apt updgrade` and `sudo apt install`.  |
| `sudo apt upgrade` | If there are new versions of currently installed packages, this will upgrade them. |
| `sudo apt install <app_names>` | Install one or more apps if not already installed |
| `source <script>` | This executes the commands in a file `<script>` within the current shell environment |
| `. <script>` | Ditto.  Notice the space after the "`.`" |
| `alias <new_name>="<command>"` | Define an alias for a command (see below) |
| `alias` | List all currently defined aliases |
| `grep -rnHI "text to find"` | Recursively searches through text files in a folder for `"text to find"` |

___
# Home Folder
On Ubuntu your home folder is typically `/home/$USER`, where `$USER` specifies an environment variable holding your username.  It is also held in `$HOME`.  As shown above, there are multiple ways to `cd` to your home folder.  

Often when you start a shell in a new terminal it will start you out in your home folder.  However, in WSL it seems that you are placed in your *Windows home folder* which is different: something like `/mnt/c/Users/<user_id>`.  It is preferable **not** to work in your Windows filesystem, so if you find yourself in this folder I recommend you start by switching to your Ubuntu home folder.  A `~` indicates your home folder as shown here:

![Home folder](.Images/home.png)

Create folders and subfolders within your Ubuntu home folder for this class.

___
### [Back to Index](../Readme.md)
