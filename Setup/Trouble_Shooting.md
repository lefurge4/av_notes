# Trouble Shooting

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)

There are a number of issues that people new to ROS often struggle with.  I intend to accumulate those issues in this document as the semester progresses.  The idea is that this page can be a go-to document if things aren't working as you expect.  Of course, there are plenty of online resources too.

# List of Issues:

1. [ROS Command Not Found](#1-ROS-Command-Not-Found)
2. [ROS Package Not Found](#2-ROS-Package-Not-Found)
3. [Tab Complete Not Working](#3-Tab-Complete-Not-Working)
4. [Could not connect to display](#4-Could-not-connect-to-display)
5. [Gazebo not running](#5-Gazebo-not-running)
6. [Error in REST request](#6-Error-in-REST-request)
7. [/usr/bin/env: ‘python\r’: No such file or directory](#7-python-r-run-error)
8. [`catkin_make`: Could NOT find PY_em (missing: PY_EM)](#8-catkin_make-Could-NOT-find-PY_em-missing-PY_EM)
9. [Failed to contact master](#9-Failed-to-contact-master)
___
# 1. ROS Command Not Found

If a ROS command is not found, for example like this:
```
$ catkin_make

Command 'catkin_make' not found, but can be installed with:

sudo apt install catkin
```

Then do **not** install it with the suggested `sudo apt install` command!  Rather, this is most likely caused by your **underlay** not being sourced.  Check it with:
```
printenv |grep ROS
```
And you should see plenty of ROS environment variables.  If not then source the underlay with:
```
source /opt/ros/noetic/setup.bash
```
___
# 2. ROS Package Not Found
If a package you created or downloaded into your workspace `~/catkin_ws` is not running, for example, this:
```
$ rosrun lab2_nodes obs_detect.py
[rospack] Error: package 'lab2_nodes' not found
```
Most likely it is because your **overlay** has not been sourced.  Source it with:
```
source ~/catkin_ws/devel/setup.bash
```
Another reason could be that the python code for your node is not an executable file.  To make it executable:
```
cd <path_to_package_src>
chmod +x <name_of_python_file.py>
```
If it is a C++ package in your workspace, make sure that you have compiled it:
```
cd ~/catkin_ws
catkin_make
```
___
# 3. Tab Complete Not Working

This is most likely because the underlay is not sourced as in: [ROS Command Not Found](#ROS-Command-Not-Found), or if it is for a package in your workspace, then the overlay may not be sourced: [ROS Package Not Found](#ROS-Package-Not-Found).  
___
# 4. Could not connect to display

You may find various Ubuntu or ROS programs will crash if they attempt to open a graphical window.  For example, this is what I got when running `rqt`:

![Display](.Images/TroubleShootDisplay.png)

There are a few things that could be wrong.  The X-server needs to be running which you can check in Windows from the task-bar icons

![x-launch](.Images/x-launch.png)

If it is not running, start it by typing `xlaunch` in the Windows search bar.

If you are still getting an error, ensure that the `DISPLAY` environment variable is set correctly, as explained in [X_Server](../Setup/Optional/X_Server.md).  You can see look at your `DISPLAY` variable with:
```
$ printenv |grep DISPLAY
DISPLAY=192.168.86.33:0
```
or
```
$ echo $DISPLAY
DISPLAY=192.168.86.33:0
```
Of course you should see the IP address of your host OS.  

If you get this error:
```
Authorization required, but no authorization protocol specified
Error: Can't open display: 192.168.86.33:0
```
Then you should quit and re-start X-Launch, and make sure to disable access control in the Extra Settings window.  

To confirm that your X11 is working, you can do:
```
sudo apt install x11-apps
xeyes
```
And you should see the xeyes window.

___
# 5. Gazebo not running

In Windows / Mac, if it is not a [Display problem](#Could-not-connect-to-display), then make your your `~/.bashrc` file has this line:
```
export LIBGL_ALWAYS_INDIRECT=0   
```
Also type it in any open terminal windows.  Then see if Gazebo runs with the command:
```
gazebo
```
Note that in Noetic you should be using Gazebo 11.  You can check with:
```
sudo apt-get list --installed | grep gazebo
```
___
# 6. Error in REST request

When running Gazebo in Melodic you may get an error like this:
````
[Err] [REST.cc:205] Error in REST request
libcurl: (51) SSL: no alternative certificate subject name matches target host name 'api.ignitionfuel.org'
````
That does not affect operation, but when running Gazebo it is *always* good to carefully review and address any startup errors.  Making sure there are no errors is likely to save you grief in the long run debugging unexpected results.  You can eliminate this error for future running of Gazebo by editing the file:`~/.ignition/fuel/config.yaml`, and replacing: `api.ignitionfuel.org` with this: `fuel.ignitionrobotics.org`.  Here is a command that will do that for you, and fix this error so it does not show up in subsequent runs:
```bash
sed -i 's/api.ignitionfuel.org/fuel.ignitionrobotics.org/g' ~/.ignition/fuel/config.yaml
```

# 7. Python `\r` run error

You may find you get the following error when running a python node:
```
$ rosrun lab4_color im_capture.py
/usr/bin/env: ‘python\r’: No such file or directory
```
This can happen when a text file has both carriage-returns and line-feeds at the end of each line.  This is the standard in Windows, whereas linux uses only line-feeds.  The solution is to edit the file, in this case `im_capture.py`, and replace the carriage-return line-feed pairs with line-feed characters.  On the bottom right of the VSCode window you'll see the following when the file has carriage-return line-feeds:

![CRLF](.Images/crlf_error.png)

It is quite easy to fix.  Simply click on the `CRLF` text and select `line-feeds` and save the file.  This will replae all the carriage-return-line-feed pairs with line-feeds.  

# 8. `catkin_make`: Could NOT find PY_em (missing: PY_EM)
Catkin_make may need to be told to use Python3, which can be done by using this argument when calling it:
```
catkin_make -DPYTHON_EXECUTABLE=/usr/bin/python3
```

# 9. Failed to contact master
When running a ROS command you get an error like one of the following:
```
[ERROR] [1610627301.332331100]: [registerPublisher] Failed to contact master at [localhost:11311].  Retrying...
```
```
ERROR: Unable to communicate with master!
```
Then most likely you are not running `roscore` in a separate thread, and you should start it.  

If it is running, but on a different computer of VM, then it may be a problem with the environment variables `ROS_MASTER_URI` and `ROS_HOSTNAME`.  To see how to set these when `roscore` is running on a Turtlebot, have a look in [ROS/Turtlebots_Gazebo.md](../ROS/Turtlebots_Gazebo.md) at section `ROS Network Configuration`.

___
### [Back to Index](../Readme.md)






