#!/usr/bin/env python
''' Clearning robot demonstrating a state machine 
    This state machine has 2 states 'CHARGE' and 'CLEAN'
    It operates in the CLEAN state while it has charge, but transitions to CHARGE when
    it needs power and then back to CLEAN

    Usage:
     rosrun vacbot vac_01_basic.py

    Daniel Morris, April 2020
'''
import rospy
import smach

class charge(smach.State):  # Define a state and outcomes
    def __init__(self):
        smach.State.__init__(self, outcomes=['full'])

    def execute(self, userdata):
        rospy.loginfo('Charging')
        return 'full'

class clean(smach.State):   # Define a state and outcomes
    def __init__(self):
        smach.State.__init__(self, outcomes=['done','cleaning','need_charge'])
        self.cleanval = 0
        self.time_since_charge = 0
    
    def execute(self, userdata):
        self.time_since_charge += 1

        if self.cleanval >= 3:
            return 'done'
        elif self.time_since_charge >= 3:
            rospy.loginfo('Need a charge')
            self.time_since_charge = 0
            return 'need_charge'
        else:
            self.cleanval += 1
            rospy.loginfo('Clean value: '+str(self.cleanval))
            return 'cleaning'


def init_vac_basic_sm():
    # create SMACH state machine
    sm = smach.StateMachine(outcomes=['finished','aborted'])
    sm.set_initial_state(['CLEAN'])

    with sm:  # This opens sm container for adding states:
        smach.StateMachine.add('CHARGE', charge(),  # Add state and transition
                transitions={'full':'CLEAN'})
        smach.StateMachine.add('CLEAN', clean(),    # Add state and transition
                transitions={'done':'finished',
                             'cleaning':'CLEAN',
                             'need_charge':'CHARGE'})
    return sm
    
if __name__ == '__main__':
    rospy.init_node('Basic_SMACH')
    sm = init_vac_basic_sm() # Create state machine    
    outcome = sm.execute()   # Execute state machine
    rospy.loginfo('Final outcome: '+outcome)


