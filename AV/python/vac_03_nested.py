#!/usr/bin/env python
''' Clearning robot demonstrating a nested state machine
    A State Machine can be treated exactly like a state,
    and so can be nested within another state machine.

    In this example a top-level state machine has one state
    that randomly decides if the cleaning robot will clean the room.
    If it decides to clean the room, it transitions to the basic
    cleaning state machine from vac_01_basic.

    Usage:
     rosrun vacbot vac_03_nested.py

    Daniel Morris, April 2020
'''
import rospy
import smach
import time
from vac_01_basic import init_vac_basic_sm  #import state machine

class rand_choice(smach.State):
    '''Randomly returns a yes or no depending on the time of day'''
    def __init__(self):
        smach.State.__init__(self, outcomes=['yes','no'])

    def execute(self, userdata):       
        rospy.loginfo('Asking robot to clean my room') 
        if int( time.time() ) % 2:
            return 'yes'
        else:
            return 'no'

def init_vac_moody():
    #create SMACH state machine
    sm = smach.StateMachine(outcomes=['clean_room','dirty_room','aborted'])
    sm.set_initial_state(['ASK_VAC'])

    with sm:
        smach.StateMachine.add('ASK_VAC', rand_choice(),  # Add a state
                transitions={'yes':'DO_ROOM',
                             'no':'dirty_room'})
        smach.StateMachine.add('DO_ROOM', init_vac_basic_sm(), # Add a state machine
                transitions={'finished':'clean_room',
                             'aborted':'aborted'})
    return sm
    
if __name__ == '__main__':
    rospy.init_node('Moody_vac')
    sm = init_vac_moody()  # Create state machine
    outcome = sm.execute() # Execute state machine
    rospy.loginfo('Final outcome: '+outcome)


