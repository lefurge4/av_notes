#!/usr/bin/env python
''' Clearning robot demonstrating a state machine 
    This example shows storing the clean and charge values in userdata
    which can be accessed by all states in a state machine

    Using userdata requires specifying input_keys (for which values can be read) 
    and output_keys (for which values can be written).  Also remapping will map
    these keys to the original userdata field names.

    Usage:
     rosrun vacbot vac_02_userdata.py

    Daniel Morris, April 2020
'''
import rospy
import smach

class charge(smach.State): # Define a state, outcomes and userdata hooks
    def __init__(self):
        smach.State.__init__(self, outcomes=['full'], 
                            input_keys=['in_charge'], output_keys=['out_charge'])
        self.delta_charge = 2

    def execute(self, userdata):
        userdata.out_charge = userdata.in_charge + self.delta_charge
        rospy.loginfo('Charge value: '+str(userdata.in_charge)) # in_charge and out_charge are the same
        return 'full'

class clean(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['done','cleaning','need_charge'],
                            input_keys=['in_charge','in_clean'], 
                            output_keys=['out_charge','out_clean'])
    
    def execute(self, userdata):
        if userdata.in_clean >= 3:
            return 'done'
        elif userdata.in_charge <= 0:
            return 'need_charge'
        else:
            userdata.out_clean = userdata.in_clean + 1
            userdata.out_charge = userdata.in_charge - 1
            rospy.loginfo('Clean: '+str(userdata.in_clean) + ', charge: ' + str(userdata.in_charge))
            return 'cleaning'


def init_vac_userdata():
    #create SMACH state machine
    sm = smach.StateMachine(outcomes=['finished','aborted'])
    sm.userdata.sm_clean = 0
    sm.userdata.sm_charge = 0
    sm.set_initial_state(['CLEAN'])

    with sm:
        smach.StateMachine.add('CHARGE', charge(), # Add state and mapping for IO hooks
                transitions={'full':'CLEAN'},
                remapping={'in_charge':'sm_charge','out_charge':'sm_charge'})
        smach.StateMachine.add('CLEAN', clean(),
                transitions={'done':'finished',
                             'cleaning':'CLEAN',
                             'need_charge':'CHARGE'},
                remapping={'in_charge':'sm_charge','out_charge':'sm_charge',
                           'in_clean':'sm_clean','out_clean':'sm_clean'})
    return sm
    
if __name__ == '__main__':
    rospy.init_node('Basic_SMACH')
    sm = init_vac_userdata()  # Create state machine
    outcome = sm.execute()    # Execute state machine
    rospy.loginfo('Final outcome: '+outcome)

