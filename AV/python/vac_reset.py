#!/usr/bin/env python
'''
     roslaunch greenline turtlebot3_greenline.launch
     roslaunch greenline start_movebase.launch    
     rosrun vacbot vac_reset.py  
'''
import rospy
import smach
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose
from smach_ros import SimpleActionState
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray
from tf.transformations import quaternion_from_euler
from transform_frames import transform_frames

class waypoints:
    def __init__(self):
        self.trans = transform_frames()

    def get_line_waypoints(self, x, y, yaw): 
        wplist = PoseArray()
        pnt = Point(x, y, 0.)
        quat = Quaternion(*(quaternion_from_euler(0, 0, yaw, axes='sxyz')))
        wplist.poses.append(Pose(pnt, quat))
        wplist_map = self.trans.pose_transform(wplist, 'odom', 'map')
        return wplist_map

def init_follower_sm(wplist):
    # create SMACH state machine
    sm = smach.StateMachine(outcomes=['succeeded', 'aborted', 'preempted'])
    # These three outcomes are standard for action states, and are the outcomes of SimpleActionState

    with sm:  # This opens sm container for adding states:
        for i, wp in enumerate(wplist.poses):
            wpose = MoveBaseGoal()
            wpose.target_pose.header.frame_id = "map"
            wpose.target_pose.header.stamp = rospy.Time.now()
            wpose.target_pose.pose = wp
            name = 'wp' + str(i)
            if i < len(wplist.poses) - 1:
                nextname = 'wp' + str(i + 1)
            else:
                nextname = 'succeeded'
            smach.StateMachine.add(name,
                                   SimpleActionState('move_base',
                                                     MoveBaseAction,
                                                     goal=wpose,
                                                     server_wait_timeout=rospy.Duration(10.0)),
                                   transitions={'succeeded': nextname})
    return sm

class readOdom:
    ''' This class subscribes to /odom and makes it available as self.pose'''

    def __init__(self):
        self.pose = Pose()
        rospy.Subscriber("/odom", Odometry, self.callback)
        rospy.sleep(0.1)  # Make sure pose has time to be initialized with callback
        rospy.loginfo('Subscribing to /odom')

    def callback(self, msg):
        '''Copies odometry pose to self.pose'''
        self.pose = msg.pose.pose

if __name__ == '__main__':

    rospy.init_node('runReset', anonymous=True)
    ro = readOdom()
    wp = waypoints()
    keepgoing = True
    while keepgoing:
        ans = int(input("Choose a spot (1,2,3,4,5,6): "))
        if ans == 1:
            wplist = wp.get_line_waypoints(-1.5, 1.5, 0.)
        elif ans == 2:
            wplist = wp.get_line_waypoints(-0.7, 1.5, -0.1)
        elif ans == 3:
            wplist = wp.get_line_waypoints(0.25, 1.05, -1.7)
        elif ans == 4:
            wplist = wp.get_line_waypoints(0.0, -0.58, -1.6)
        elif ans == 5:
            wplist = wp.get_line_waypoints(0, -1.58, 0.1)
        elif ans == 6:
            wplist = wp.get_line_waypoints(0.78, -1.6, 0.1)
        elif ans == -100:
            wplist = wp.get_line_waypoints(1.5, -0.5, 0.)  # drive into red region (test)
        else:
            print("Not one of the number 1-6, quitting")
            keepgoing = False
            continue
        sm = init_follower_sm(wplist)  # Create state machine
        outcome = sm.execute()  # Execute state machine
        rospy.loginfo('Final outcome: ' + outcome)
