# Deep Neural Networks

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)

* [Inference with OpenCV](#Inference-with-OpenCV)
* [Integration with ROS](#Integration-with-ROS)
___
# Inference with OpenCV

OpenCV provides a very simple and convenient way to run pre-trained Deep Nerual Networks (DNNs).  It does not have the tools for training them, but can load a pre-trained network, apply it to image data and display the result.  Here's the output from a short OpenCV code segment running a DNN:

![Detections](.Images/bridge_detect.png)

A simple way to learn how to use OpenCV is with an example, which we take from this [page](https://gist.github.com/YashasSamaga/e2b19a6807a13046e399f4bc3cca3a49).  Here is the code:

```python
# From: https://gist.github.com/YashasSamaga / yolov4.py
import cv2
import time

CONFIDENCE_THRESHOLD = 0.2
NMS_THRESHOLD = 0.4
COLORS = [(0, 255, 255), (255, 255, 0), (0, 255, 0), (255, 0, 0)]

class_names = []
with open("classes.txt", "r") as f:
    class_names = [cname.strip() for cname in f.readlines()]

vc = cv2.VideoCapture("demo.mp4")

net = cv2.dnn.readNet("yolov4.weights", "yolov4.cfg")
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA_FP16)

model = cv2.dnn_DetectionModel(net)
model.setInputParams(size=(416, 416), scale=1/255)

while cv2.waitKey(1) < 1:
    (grabbed, frame) = vc.read()
    if not grabbed:
        exit()

    start = time.time()
    classes, scores, boxes = model.detect(frame, CONFIDENCE_THRESHOLD, NMS_THRESHOLD)
    end = time.time()

    start_drawing = time.time()
    for (classid, score, box) in zip(classes, scores, boxes):
        color = COLORS[int(classid) % len(COLORS)]
        label = "%s : %f" % (class_names[classid[0]], score)
        cv2.rectangle(frame, box, color, 2)
        cv2.putText(frame, label, (box[0], box[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    end_drawing = time.time()
    
    fps_label = "FPS: %.2f (excluding drawing time of %.2fms)" % (1 / (end - start), (end_drawing - start_drawing) * 1000)
    cv2.putText(frame, fps_label, (0, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 2)
    cv2.imshow("detections", frame)
```
To run this requires OpenCV 4.5, as explained in Lab 6. 

Next, let's examine each section of this code:
```python
CONFIDENCE_THRESHOLD = 0.2
NMS_THRESHOLD = 0.4
```
The confidence threshold indicates the minimum probability of target for the method to return a detection.  NMS refers to non-maximal suppression.  Detectors often return multiple overlapping detections for each target.  NMS will keep only the strongest (or most confident) detection.  The threshold is a minimum overlap required to suppress a detection.

```python
class_names = []
with open("classes.txt", "r") as f:
    class_names = [cname.strip() for cname in f.readlines()]
```
This loads the text names for each target class into an array
```python
net = cv2.dnn.readNet("yolov4.weights", "yolov4.cfg")
```
A DNN is defined by a set of operations arranged in an archtecture.  These operations consist of a sequence of convolutions, multiplications, pooling, padding etc. are specified in the `.cfg` file.  For example, a `.cfg` file could define an architecture like this:

![YOLOv1](.Images/remotesensing-12-02501-g002.png)

This is YOLOv1, and taken from: [https://www.mdpi.com/2072-4292/12/15/2501/htm](https://www.mdpi.com/2072-4292/12/15/2501/htm).  

Many of the operations have trainable parameters or weights, which are stored in the `.weights` file.  Most of these weights belong to the convolutional layers and fully connected layers.  During training, the values of the weights are optimized, and during inference they simply parameterize the network.  There may be tens of thousands or millions of weights in a DNN, and so these `.weights` files can be quite large.  

```python
model = cv2.dnn_DetectionModel(net)
model.setInputParams(size=(416, 416), scale=1/255)
```
OpenCV supports a number of DNN architectures with a detection class.  This can apply the network to an image, do NMS, and generate an output, i.e. do inference.  Here the detection class is being initialized, and the input image size is specified `416x416`.  Many networks require a specified input image size, and the detection class will handle resizing an image to match the network input and resizing the output detections to match the raw image.

```python
classes, scores, boxes = model.detect(frame, CONFIDENCE_THRESHOLD, NMS_THRESHOLD)
```
Here inference is performed on an image generating a set of detections which are each specified by a class, a score and a box.  
```python
for (classid, score, box) in zip(classes, scores, boxes):
    color = COLORS[int(classid) % len(COLORS)]
    label = "%s : %f" % (class_names[classid[0]], score)
    cv2.rectangle(frame, box, color, 2)
    cv2.putText(frame, label, (box[0], box[1] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
```
This plots a rectangle for each detection box and labels it with both its class and score.  (While it calls it score, I believe it is the sigmoid of the score and so closer to a probability of detection.)

# Integration with ROS

In Lab 6 you will start with the above OpenCV example and build a ROS node that runs a DNN doing object detection.  

___
### [Back to Index](../Readme.md)






