# ECE-CSE 434 Autonomous Vehicles Course: Build

Notes for: **Fall Semester, 2021**
___
### Instructor and author: 
### Daniel Morris, PhD, Associate Professor, https://www.egr.msu.edu/~dmorris

Copyright (C) 2020 - 2021
___

The documents in this repository are class notes for the **Build** portion of *ECE-CSE 434 Autonomous Vehicles*.  They will guide you through learning ROS as well as implementing some key topics in Autonomous Vehicles.  The style is hands-on, and intended for you to be following along and entering the commands onto your own computer.  I believe you'll learn best if you try out the examples as you read them.

In addition to being a tutorial, these documents are also a troubleshooting reference.  For beginners it is easy to forget or skip important steps when building or running ROS packages.  If something is not working that you think should be working, head to the appropriate section and see what you did differently.  

# 1. Setup Your Environment

The following are installation instructions to get the software you need for the class.

* [Setup Ubuntu](Setup/Ubuntu.md):  A pre-configured remote Ubuntu environment
* [Python Environment](Setup/Python_Environment.md): Configure Python to use the ROS environment and libraries such as OpenCV
* [VSCode](Setup/VSCode.md): My preferred IDE to debug Python and ROS code.  Debugging examples are included.
* [Trouble Shooting](Setup/Trouble_Shooting.md): Lists common issues and solutions for those learning ROS
* [Assignments and Git](Setup/Assignments_Git.md): Basics for using Git in this class, including receiving and submitting assignments

___
# 2. Python Review

* **[Python Notes Repo](https://github.com/dmorris0/python_intro/blob/main/README.md)**
  * A separate repository that provides a quick, intense review of the Python background you will need for this course.  You can optionally install Python on your own PC as described in these notes, or else use the provided VDI Remote Desktop with Python and VSCode already installed as described above.

___
# 3. ROS 1

* **1. [Start ROS](ROS/ROS_Start.md)** 
  * ROS Prerequisites
  * Create a Workspace
  * Create ROS Packages
  * Example ROS 1 Publisher and Subscriber Package
* **2. [ROS Messaging](ROS/Messaging.md)** 
  * ROS Messaging
  * ROS Topics Details
  * Defining Your Own Message Type
* **3. [Turtlebots and Gazebo](ROS/Turtlebots_Gazebo.md)** 
  * Turtlebots as Mobile Platforms
  * Turtlebots in Gazebo
  * Example Node with Arguments: Drive in a Circle
  * Odometry
  * Real Turtlebots
* **4. [ROS with Multiple Threads and OpenCV](ROS/threads.md)**
  * Mutliple Threads
  * Call-Backs
  * Locks
  * OpenCV and Threads
  * Examples of Multi-Threaded Nodes and OpenCV
* **5. [ROS Launch and Parameters](ROS/launch.md)** 
  * Launching Nodes
  * Example Launch Files
  * ROS Parameters
* **6. [Coordinate Transforms](ROS/Coordinate_Transforms.md)**
  * Coordinate Systems
  * Transforming Frames with Examples
  * Publishing Static Transforms

___
# 4. AV Topics
* **1. [Colored Object Detection](AV/Color_Detect.md)** 
  * Logistic Regression
* **2. [Deep Neural Networks](AV/dnns.md)** 
  * Inference with OpenCV
  * Integration with ROS
* **3. [SLAM and Navigation in the Greenline World](https://gitlab.msu.edu/av/greenline)** 
  * Start the World
  * Run SLAM and Command Turtlebot
  * Planning and Navigation
* **4. [State Machines](AV/State_Machines.md)** 
  * Create `vacbot` Package
* **5. [Tracking and Prediction](AV/Tracking.md)** 
  * Kalman Filter
* **6. [PID (Proportional, Integral, Derivative) Line Follower](AV/PID.md)** 
  * PID Controller
  * Line Follower

