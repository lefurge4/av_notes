#!/usr/bin/env python
'''
    secrets_sub.py 

    This is a ROS node that subscribes to a topic and displays messages in a rotating window
    Key things to notice on this code:
       -- Subscribes to a String topic
       -- Uses a lock in the callback to be thread safe
       -- Uses OpenCV for plotting, and all within one thread
    Unlike in secrets_sub_simple.py, the lock enables the callback and plotting to occur
    independently of each other

    See also: secrets_pub.py, secrets_sub_simple.py av_notes/Setup/python/hello_user.py

    Daniel Morris, Oct 2020
'''
import rospy
from std_msgs.msg import String
from hello_user import ImageRotator
from threading import Lock

class MessageDisplay:

    def __init__(self, topic_name='secrets', node_name='secret_finder'):
        self.message = 'No message yet'
        self.message_changed = False
        self.message_lock = Lock()                    # A lock to keep the message safe from multiple threads
        self.imr = ImageRotator( self.message )      # This class handles OpenCV plotting functions
        rospy.init_node(node_name, anonymous=True)    # Initialize ROS node
        rospy.Subscriber(topic_name, String, self.read_callback)  # Subscribe to topic of strings
        rospy.loginfo('Subscribing to topic: ' + topic_name)

    def read_callback(self, msg):
        ''' Call-backs with the subscribed message use a separate thread from the main thread
        '''
        with self.message_lock:           # Keep following block thread-safe
            self.message = msg.data       # Message only copied when safe
            self.message_changed = True   # This variable also kept thread-safe

    def display(self):
        ''' This displays the most recently read message in a rotating window '''
        rate = rospy.Rate(30)             # Update OpenCV plots at up to 30 Hz
        while self.imr.show_next_image():
            with self.message_lock:       # Keep following block thread-safe
                if self.message_changed:  # This variable accessed only when lock is acquired 
                    self.imr.create_hello_image(self.message)
                self.message_changed = False
            rate.sleep()

if __name__=="__main__":

    md = MessageDisplay()       # Initialize the node and subscriber callback
    md.display()                 # Call OpenCV functions from the main thread


