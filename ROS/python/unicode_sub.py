#!/usr/bin/env python
'''
    unicode_sub.py 

    This is a ROS node that subscribes to a topic containing Int32 elements
    Each integer is assumed to be unicode, and converted to a character and output on the terminal

    See also: unicode_pub.py

    Daniel Morris, Oct 2020
'''
import rospy
from std_msgs.msg import Int32

class UniDecoder:

    def __init__(self, topic_name='uni_char', node_name='uni_sub'):
        rospy.init_node(node_name) # Initialize ROS node
        # Create a subscriber that reads an Int32 from the topic and sends this to self.read_callback
        rospy.Subscriber(topic_name, Int32, self.read_callback)
        rospy.loginfo('Subscribing to topic: ' + topic_name)

    def read_callback(self, msg):
        ''' Decodes a single unicode character and outputs it to the terminal '''
        print(chr(msg.data),end='')

if __name__=="__main__":

    mydecoder = UniDecoder()  # Create a subscriber that reads and decodes the topic
    rospy.spin() # Wait and perform callbacks until node quits

