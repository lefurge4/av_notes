# ROS with Multiple Threads and OpenCV

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)
## Contents

* [Mutliple Threads](#Mutliple-Threads)
* [Call-Backs](#Call-Backs)
* [OpenCV and Threads](#OpenCV-and-Threads)
* [Example Package](#Example-Package)
  * [Example of OpenCV in a Call-Back](#Example-of-OpenCV-in-a-Call-Back)
* [Locks](#Locks)
  * [Example of OpenCV with Locks](#Example-of-OpenCV-with-Locks)

___
# Multiple Threads

While ROS takes much of the pain out of multi-threaded programming by running nodes asynchronously, there are nevertheless cases where there will be multiple threads within a node.  It is important to be cognisant of when this happens, and to properly handle these cases.  Failing to account for potential memory conflicts between threads can lead to data corruption, incorrect output and code crashes.  How much and how frequent will depend on the case, but any amount should seriously concern a developer.  No one wants a memory error to cause vehicle failures or crashes, and so it is important to practice safe threading.  

___
# Call-Backs

This document is not a full treatment of threads in ROS.  Rather it considers a common case where ROS nodes have multiple threads; **call-back functions**.  As we've seen in the ROS introduction, subscribing to topics is usually done with a callback.  This allows your code to react whenever there is a published topic.  The flip side, is that the callback with run in a separate thread to the rest of the node.  If your callback is doing something isolated from the rest of the node, such as outputting text to the terminal, then there may be no memory issues.  However, as we'll see, there are various ways in which memory conflicts can occur.

The opportunity for memory corruption or operation failure is when two threads attempt to read or write the same block of memory at the same time.  In a single-threaded program this generally does not happen, but it can easily occur when you have multiple threads.  A good example is if a call-back function is part of a class.  Then if the call-back changes or uses data fields of the class at the same time these are being used outside the call-back, that is when there may be a memory conflict.  

One might ask, can call-backs conflict with each other?  While different call-backs may conflict, a particular call-back will block further calls to itself until it has completed.  So if a call-back is changing a variable, it does not have to be concerned about another intance of itself changing the same variable at the same time.  


___
# OpenCV and Threads
The main thing to know about OpenCV is that its functions and objects are not thread safe.  The reasons, I presume, have to do with computational speed.  The implication for your ROS nodes is that if you use OpenCV, all OpenCV objects should be accessed and set within the **same** thread.  And this includes any plots or figures.  

So how do you ensure all your OpenCV objects are accessed within the same thread?  Recall that call-backs use separate threads from the main block of code.  That means your OpenCV objects should all be handled either outside your call-back or all handled inside your call-back.  (A call-back will prevent multiple instances of itself running at the same time.)

Keeping all usage of OpenCV blocks within the same thread can lead to some awkward-looking code.  For example, if you use OpenCV inside a call-back, then that call-back needs to do everything including intializing all OpenCV variables and all OpenCV plotting.  Ensuring that you are not accessing these OpenCV objects outside the call-back can be a bit tricky.  Nevertheless, by following this rule of thumb you should keep your OpenCV code thread safe.

___
# Example Package

The following ROS package illustrates handling of two threads and OpenCV. There are two versions: a simple version that uses no locks, and a more complex version that uses locks.

Start by creating a package called `secrets` as follows:
````
cd ~/catkin_ws/src
catkin_create_pkg secrets rospy
````
Next copy the following four python code examples into this package [av_notes/ROS/python/secrets_pub.py](python/secrets_pub.py), [av_notes/ROS/python/secrets_sub_simple.py](python/secrets_sub_simple.py), [av_notes/ROS/python/secrets_sub.py](python/secrets_sub.py), and [av_notes/Setup/python/hello_user.py](../Setup/python/hello_user.py) as follows:

````
cd ~/catkin_ws/src/secrets/src
cp ~/<path_to_my_repo>/av_notes/ROS/python/secrets* .
cp ~/<path_to_my_repo>/av_notes/Setup/python/hello_user.py .
````
Make the nodes executable.  In the above `secrets/src` folder type:
````
chmod +x secrets*
````
Before running your node, start `roscore` in a shell.  Also make sure that your X-server has been launched (if you are in Windows).  
## Example of OpenCV in a Call-Back

To run the simple OpenCV example, in one terminal type:
````
rosrun secrets secrets_pub.py
````
And in another terminal type:
````
rosrun secrets secrets_sub_simple.py
````
The subscriber node will display a window showing secret messages passed from the publisher node like this:

![Displayed secrets](.Images/secrets.jpg)

Let's see how this works.  Here is the publisher, `secrets_pub.py`, which is very simple and uses a single thread.  It simply publishes a random string to a topic called `secrets` at a rate of 1 message per second:
```python
#!/usr/bin/env python
import rospy
import numpy as np
from std_msgs.msg import String      # Will publish a topic of type String

class PubRandomMessages:

    def __init__(self, message_list, topic_name='secrets', node_name='secret_sender'):        
        self.message_list = message_list                              # Store messages here
        rospy.init_node(node_name)                                    # Initialize ROS node
        self.pub = rospy.Publisher(topic_name, String, queue_size=1)  # Create publisher
        rospy.loginfo('Publishing secret messages to topic: ' + topic_name )
        self.run()                                                    # Run until stopped

    def random_message(self):
        ''' Returns a random message from list '''
        return self.message_list[np.random.randint(len(self.message_list))]

    def run(self):
        rate = rospy.Rate(1)    # ROS set a loop rate of maximum 1 Hz
        while not rospy.is_shutdown():
            self.pub.publish( self.random_message() )  # Publish message
            rate.sleep()        # This sleeps to ensure loop rate is at most 1 Hz
    
if __name__=="__main__":

    secret_messages=['Hello World','The meaning of life','42',"Can't ROS that"]
    PubRandomMessages( secret_messages )
```
The only thing that is new here is using the `rospy.Rate()` function to keep a loop running at a fixed rate.  Of course it won't stop the loop going slower than this rate if the functions in it take more time than expected.

Next let's look at the subscriber, `secrets_simple_sub.py`.  
```python
#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from hello_user import ImageRotator
from threading import Lock

class MessageDisplay:

    def __init__(self, topic_name='secrets', node_name='secret_finder'):
        self.imr = None                             # Allocate space for ImageRotator, but don't initialize until in subscriber
        rospy.init_node(node_name,anonymous=True)   # Initialize ROS node
        rospy.Subscriber(topic_name, String, self.read_callback)  # Subscribe to topic of strings
        rospy.loginfo('Subscribing to topic: ' + topic_name)

    def read_callback(self, msg):
        ''' Call-backs with the subscribed message use a separate thread from the main thread
        '''
        if self.imr is None:
            self.imr = ImageRotator()      # Initialize in callback since this class handles OpenCV plotting functions

        self.imr.create_hello_image( msg.data )
        self.imr.show_next_image()


if __name__=="__main__":

    md = MessageDisplay()        # Initialize the node and subscriber callback
    rospy.spin()                 # Wait for callbacks
```
First, notice that we are using the `ImageRotator()` class imported from `hello_user.py`.  We'll rely on the OpenCV functions in this for displaying the message.  

Now the callback function is a separate thread than the main function, so to stay threadsafe while using OpenCV, we put all OpenCV calls within the callback.   This includes the initialization call to `ImageRotator()`.  Each time a message is received on the topic, the callback rotates it and displays it in the window.

This is a simple way to keep your OpenCV functions threadsafe.  But it has the limitation that we are only calling OpenCV during a callback.  The result is that we can only rotate the message during a callback.  It would be nice to be rotating the image between callbacks to get a continuous rotation rather than a jerky, step-like rotation, and that is what we look at next.

___
# Locks

There are various strategies and tools for multi-threaded programming, and here I will introduce an easy-to-use method that will be sufficient for this course.  Locks are simple way to protect memory with multiple threads.  A lock is a mechanism that ensures that only one thread at a time can access a block of code.  How it works is best explained with an example.  Let's say `my_object` is an object that may be accessed from multiple threads.  First thing to do is create a lock for it:  
```python
from threading import Lock
my_object_lock = Lock()              # Create a lock for my_object
```
Here we created a lock for `my_object` and creatively called it `my_object_lock` (you can call it whatever you like).  Now to protect `my_object` from multiple threads we surround any access of it with a locking mechanism as follows:
```python
my_object_lock.acquire()             # Block further execution until acquired
my_object = calculate_new_value()    # Now it is safe to change my_object
my_copy = my_object.copy()           # We can make a copy of it here too
my_object_lock.release()             # Now that we're done, release our lock
```
Before we either read or write to `my_object`, we always first call `my_object_lock.acquire()`.  This function will block further execution of our code if another thread has already acquired the lock and will only enable our code to proceed when the other thread had released the lock.  Once our code has acquired the lock, no other thread can simultaneously acquire the lock and so we can safely read from or write to `my_object`.  The example shows writing to and copying `my_object`.  Then once we are done we call `my_object_lock.release()`, and other threads can now access `my_object`.

It can be a bit tedious to call `acquire()` and `release()` each time a lock is used, and this creates an opportunities for introducing bugs by forgetting to call `release()`.  A cleaner way to use a lock is using a `with` statement like this:
```python
with my_object_lock:                     # Calls acquire() and release() around the following block
    my_object = calculate_new_value()    # It is safe to change my_object in this block
    my_copy = my_object.copy()           # We can make a copy of it here too
```
Here `with my_object_lock:` will `acquire()` and so wait until the lock is acquired, run the subsequent block of code, and then call `release()`.  

**One more thing to be careful about**: The expression `a = b` in python can sometimes copy the contents of `b` into `a`, but other times will instead make `a` a pointer (or handle for) `b`.  In the latter case, if you change `b` after this expression, then it will also change `a`.  A couple common cases of the `=` operator creating handles are with lists and numpy arrays.  That is why in the above example I did not write `my_copy = my_object`. If `my_object` is a numpy array then in this case `my_copy` would be a handle to `my_object`.  Then if our code later used `my_copy` it would be bypassing the locking mechanism and would put us in danger of multiple threads accessing the same memory at the same time.

**Good Style**: If you are going to access `my_object` from multiple locations in your code, a nice way to do this is to create a setter and getter that share a lock and take care of acquiring and releasing it.  That way you can hide this complexity from your code and be confident that `my_object` is always protected from memory conflicts.

## A Very Simple Example with Locks

Consider the following diagram where we have two threads in the same process.  There are three variables in memory: `a`, `b`, and `c`.  The main thread accesses `a` and `b`, while the callback thread accesses `b` and `c` as shown here:

![Sharing Memory](.Images/multi_threads.png)

The important thing is that neither thread accesses `b` at the same time, and a way to ensure this is to use the locking mechanism described above.  Here is some code that does that:
```python
#!/usr/bin/env python
from threading import Lock
import rospy
from std_msgs.msg import Int32

class Example():

    def __init__(self):
        self.a = 1              # Only accessed in main thread
        self.b = None           # Accessed in main thread and callback
        self.c = None           # Only accessed in callback
        self.lock_for_b = Lock()        
        rospy.Subscriber('mytopic',Int32,self.callback)

    def run(self):              # Only called in main thread
        self.a += 1             # No lock
        with self.lock_for_b:
            print(self.b)           # Accessing shared memory

    def callback(self, msg):    # Called in separate thread
        self.c = msg.data       # Only accessed by callback, no lock
        print(msg.data)         # msg is only accessed in callback -- no lock
        with self.lock_for_b:
            self.b = msg.data       # self.b is also accessed by the main thread
```
In general, locks should be used for as short a time as possible (to minimize blocking other threads), so it is best to only put locks around the code where one actually reads or writes the shared memory.  That is, keep the code within the `with lock` blocks as short as possible.

## Example of Callback with Locks

Now back to our OpenCV example.  If you wish to have more control over your OpenCV functions, you can put them in a separate thread from the callback.  However, then we need to store data from the callback to be displayed by OpenCV.  This is tricky since two threads will be accessing this data: the callback and the display thread, and separate threads should not access the same data at the same time.   

Our new example can be run in a separate shell:
````
rosrun secrets secrets_sub.py
````
Here is the code in the new subscriber, `secrets_sub.py`:
```python
#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from hello_user import ImageRotator
from threading import Lock

class MessageDisplay:

    def __init__(self, topic_name='secrets', node_name='secret_finder'):
        self.message = 'No message yet'
        self.message_changed = False
        self.message_lock = Lock()                    # A lock to keep the message safe from multiple threads
        self.imr = ImageRotator( self.message )      # This class handles OpenCV plotting functions
        rospy.init_node(node_name, anonymous=True)    # Initialize ROS node
        rospy.Subscriber(topic_name, String, self.read_callback)  # Subscribe to topic of strings
        rospy.loginfo('Subscribing to topic: ' + topic_name)

    def read_callback(self, msg):
        ''' Call-backs with the subscribed message use a separate thread from the main thread
        '''
        with self.message_lock:           # Keep following block thread-safe
            self.message = msg.data       # Message only copied when safe
            self.message_changed = True   # This variable also kept thread-safe

    def display(self):
        ''' This displays the most recently read message in a rotating window '''
        rate = rospy.Rate(30)             # Update OpenCV plots at up to 30 Hz
        while self.imr.show_next_image():
            with self.message_lock:       # Keep following block thread-safe
                if self.message_changed:  # This variable accessed only when lock is acquired 
                    self.imr.create_hello_image(self.message)
                self.message_changed = False
            rate.sleep()

if __name__=="__main__":

    md = MessageDisplay()       # Initialize the node and subscriber callback
    md.display()                 # Call OpenCV functions from the main thread
```

Notice that the OpenCV functions are in the main thread and the callback has no OpenCV functions in it.  The challenge here is that both the callback and OpenCV functions need to access `self.message`, and so locks must be put around this to avoid simultaneous access from different threads.

Next note the lock, `self.message_lock` we created to control access to our stored message in `self.message`.  Both the `read_callback()` and the `display()` functions, which are in separate threads, access `self.message`.  To do this safely we created the lock, `self.message_lock`, and ensured that any access to `self.message` is within a `with self.message_lock:` block.  This prevents these separate threads from simulataneously access `self.message`.  (Note, I also included a logical variable `self.message_changed` within the lock-protected region so that `create_hello_image()` only needs to be called when a new message is read.)  

By running OpenCV outside the callback we are able to rotate the image at 30 Hz.  Using locks enables our code to independently update the messages with a callback.

___
### [Back to Index](../Readme.md)






