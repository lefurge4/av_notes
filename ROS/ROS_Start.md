# Start Programming with ROS

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
### [Index](../Readme.md)
## Contents
1. [ROS Prerequisites](#1-ROS-Prerequisites)
   * 1.1 [Underlay](#1.1-Underlay)
   * 1.2 [Overlay](#1.2-Overlay)
   * 1.3 [ROS Master](#1.3-ROS-Master)
2. [Create a Workspace](#2-Create-a-Workspace)
3. [Create ROS Packages](#3-Create-ROS-Packages)
   * 3.1 [Create a Package](#3.1-Create-a-Package)
   * 3.2 [Build a Package](#3.2-Build-a-Package)
   * 3.3 [Install Dependencies with `rosdep`](#3.3-Install-Dependencies-with-rosdep)
4. [Example ROS 1 Publisher and Subscriber Package](#4-Example-ROS-1-Publisher-and-Subscriber-Package)
   * 4.1 [The Publisher](#4.1-The-Publisher)
   * 4.2 [The Subscriber](#4.2-The-Subscriber)
   * 4.3 [Run the Package](#4.3-Run-the-Package)
5. [Questions](#5-Questions)
___

Here are preliminaries for getting started with ROS Noetic after you have installed it, see [Setup/Optional/ROS](../Setup/Optional/ROS.md). This introduces creating a workspace, and creating and testing a ROS package.   You can find extensive ROS 1 documentation online at [wiki.ros.org](http://wiki.ros.org).  


# 1 ROS Prerequisites

There are a few prerequisites before you can run ROS commands which are described in this section.   These are:
1. Source the underlay 
2. Source the overlay
3. Run `roscore` in a separate shell

## 1.1 Underlay

The **underlay** specifies the version of ROS available to the current shell.  You can source the underlay with the command:
````
source /opt/ros/noetic/setup.bash
````
This initializes ROS environment variables needed by all ROS commands including the ROS Master process.   You can see the environment variables this sets using:
````
$ printenv | grep ROS
ROS_VERSION=1
ROS_PYTHON_VERSION=3
ROS_PACKAGE_PATH=/opt/ros/noetic/share
ROSLISP_PACKAGE_DIRECTORIES=
ROS_ETC_DIR=/opt/ros/noetic/etc/ros
ROS_MASTER_URI=http://localhost:11311
ROS_ROOT=/opt/ros/noetic/share/ros
ROS_DISTRO=noetic
````
Actually, this `printenv | grep ROS` command is an easy way to confirm that the underlay has been sourced.  Notice in particular the `ROS_PACKAGE_PATH` environment variable.  After sourcing the underlay it is `ROS_PACKAGE_PATH=/opt/ros/noetic/share`.  This tells ROS to look for Noetic packages when you run ROS commands.  Sourcing the underlay is sufficient for running pre-installed ROS commands, such as `roscore`, and `catkin_make`.  If you run these without sourcing the underlay you are likely to get an error like this: ![roscore error](.Images/roscore.png) 

If you get an error like this, **Do Not Install python3-roslaunch**.  Instead, check your ROS environment with `printenv | grep ROS`, and you'll likely find you forgot to source the underlay as shown above, which you should do now.

In conclusion *always source the underlay* in order to run ROS.  One way to do this is to add the above source command above to your `~/.bashrc` file.  This is fine if you are only going to run ROS Noetic, but is not suitable if you want to run other ROS distributions in different shells.

## 1.2 Overlay

When you want to run your *own* ROS packages (as opposed to the packages in `/opt/ros/noetic`) you will need to tell ROS where your workspace is.  This is done by sourcing an **overlay** in your workspace.  If your workspace is `~/catkin_ws`, as created in the next section, then to source the overlay type:
````
source ~/catkin_ws/devel/setup.bash
````
If you have not created a workspace yet, then you'll do this after you create it.  If your workspace is another folder, simply source the `devel/setup.bash` file that workspace.  

Internally, the overlay will source the underlay function, so if you source the overlay you do not need to also source the underlay.  Have a look at your ROS parameters with `printenv | grep ROS`.  You should find the `ROS_PACKAGE_PATH` now includes your workspace as well as the the base distro.  I get the following: `ROS_PACKAGE_PATH=/home/dmorris/catkin_ws/src:/opt/ros/noetic/share`.  We can see that the new workspace is listed *ahead* of the default ROS packages.  The result is that if you name any of your packages the same name as a default ROS package, your package will take precedence.  

If you are attempting to run one of your own packages, and ROS is unable to find it, there is a good chance that you forgot to source your overlay.  On the other hand, if you are only going to run default ROS packages including those installed by `sudo apt install`, then sourcing the underlay is sufficient.

## 1.3 ROS Master

In ROS 1, the Master process acts as a master-of-ceremonies for ROS nodes.  It provides ports for nodes to communicate with each other, including publishing and subscribing to topics, and is required to be running all the time nodes are communicating.  Technical details are available here: [http://wiki.ros.org/ROS/Technical%20Overview](http://wiki.ros.org/ROS/Technical%20Overview).  To start the Master process, in a shell first **source the underlay** and then type:
```
roscore
```
You should get an output like this:
```
$ roscore
... logging to /home/dmorris/.ros/log/e372f576-5a73-11eb-9a7a-00155dd5c792/roslaunch-DM-A-19916.log
Checking log directory for disk usage. This may take a while.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://localhost:38509/
ros_comm version 1.15.9


SUMMARY
========

PARAMETERS
 * /rosdistro: noetic
 * /rosversion: 1.15.9

NODES

auto-starting new master
process[master]: started with pid [19924]
ROS_MASTER_URI=http://localhost:11311/

setting /run_id to e372f576-5a73-11eb-9a7a-00155dd5c792
process[rosout-1]: started with pid [19937]
started core service [/rosout]
```
If it runs with **no errors** you should be good to go for a single-computer system.  Leave it running in this shell and run your other ROS commands in different shells.  If you wish nodes from multiple computers to communicate, you'll need to update the `ROS_MASTER_URI` and `ROS_HOSTNAME` environment variables.  To stop roscore, use `crtl-c`.

Note that if [`roslaunch` is used to start ROS packages](launch.md) as opposed to `rosrun`, then this will automatically start `roscore` if it is not already started.

# 2 Create a Workspace

Any packages you create will need to be in a catkin workspace.  You can create and use multiple workspaces.  Before creating a workspace, source the underlay:
````
source /opt/ros/noetic/setup.bash
````

Now create a workspace for your code, typically called `catkin_ws` as follows:
````
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws
catkin_make
````

Here `catkin_make` will create `src/CMakeLists.txt`, as well as create `build` and `devel` folders in `~/catkin_ws`.  Once your workspace is created you can make it current by sourcing the overlay:
````
source ~/catkin_ws/devel/setup.bash
````

# 3 Create ROS Packages

## 3.1 Create a Package
There is a ROS wiki tutorial on creating catkin packages [ROS Package Creation](http://wiki.ros.org/catkin/Tutorials/CreatingPackage).  Packages should be created in the `src` folder, or a subfolder of this, in your workspace.  Thus to create a package do:
````
cd ~/catkin_ws/src/<any_subfolder>
catkin_create_pkg <package_name> <dependency_1> <dependency_2> ...
````
For labs in this class, `<any_subfolder>` will be your individual repo which you will use to submit your packages.  You will insert your own package name in `<package_name>`.  Any number of dependencies can be specified, but for this course the main dependency we need is `rospy` which gives access to the ROS commands needed for your Python nodes.  

For Python-based packages, there is often no need to build a package after it is created.  You can simply add the Python files to the package `src` folder.  On the other hand, C++ packages and those with new message types need to be built, as described next.

## 3.2 Build a Package
If your package contains C++ or a new message type, then you will need to build it.   Building packages is done with `catkin_make`, and good documentation is here: [http://wiki.ros.org/ROS/Tutorials/BuildingPackages](http://wiki.ros.org/ROS/Tutorials/BuildingPackages).  To build all your packages simply `cd` to your workspace and call `catkin_make` as follows:
```
cd ~/catkin_ws
catkin_make
```

In some cases, packages will need dependencies to build.  If you build command fails from dependencies not being installed, you can address that with `rosdep` described next.

## 3.3 Install Dependencies with `rosdep`
If the above build command does not work due to missing dependencies, you can install these with [rosdep](http://wiki.ros.org/rosdep).  

### Rosdep Install
If it is not installed, install it with:
```
sudo apt install python3-rosdep
```
Note: do **not** install `python3-rosdep2`, as it creates all kinds of problems and may require re-installing ROS.  After installation, `rosdep` must be initialized with:
```
sudo rosdep init
```

### Rosdep Usage
Before using `rosdep`, you should bring it up-to-date by calling:
````
rosdep update
````
Note: do **not** use `sudo` with this command.  This only needs to be done once.  Then `rosdep` can be used to install dependencies for a package with the command:
```
rosdep install <package_name>
```
Once your dependencies are installed, `catkin_make` can be used to build your packages.

# 4 Example ROS 1 Publisher and Subscriber Package

Here, let's call our package `uni_demo`, and since it will be written in Python, it will depend on the `rospy` library.   To create this package type:
````
cd ~/catkin_ws/src
catkin_create_pkg uni_demo rospy
````
Here is the output I get:
````
Created file uni_demo/package.xml
Created file uni_demo/CMakeLists.txt
Created folder uni_demo/src
Successfully created files in /home/dmorris/catkin_ws/src/uni_demo. Please adjust the values in package.xml.
````
**Important** Do not use capital letters in a package name such as: `uniDemo`  **This is a bad package name** and will cause you grief later.  

Our package will start with two nodes: a publisher and a subscriber.  Code for these is available here: [av_notes/ROS/python/unicode_pub.py](python/unicode_pub.py) and [av_notes/ROS/python/unicode_sub.py](python/unicode_sub.py).

Your source code for the package needs to be in the `uni_demo` folder, and conventionally it is put in `uni_demo/src`.  So copy these Python files to the `uni_demo/src` folder:
````
cd ~/catkin_ws/src/uni_demo/src
cp <path_to_my_repo>/av_notes/ROS/python/unicode* .
````

One further requirement for ROS package nodes is that the code be executable.  Use `cd` to switch to the folder where your python code is.  Then execute the command:
````
chmod +x unicode_pub.py unicode_sub.py
````
If ROS is unable to find or run your Python code, one of the most common reasons is that it is not execuable.  

## 4.1 The Publisher

Before running our package, let's understand it how the ROS code works.   Here is the publisher:
```python
#!/usr/bin/env python
'''
    unicode_pub.py 

    This is a ROS node that demonstrates publishing Int32 values to a topic
    The class inputs a character, converts it to unicode and publishes it as a topic
    This is not a practical string publisher (one would use the string type for that)

    See also: unicode_sub.py

    Daniel Morris, Oct 2020
'''
import rospy
from std_msgs.msg import Int32

class UniEncoder:

    def __init__(self, topic_name='uni_char', node_name='uni_pub'):
        rospy.init_node(node_name) # Initialize ROS node
        # Create a publisher that publishes to topic 'uni_char'
        self.pub = rospy.Publisher(topic_name, Int32, queue_size=10)
        rospy.loginfo('Publishing to topic: ' + topic_name )
        
    def pub_string(self, pstring):
        ''' Input a string and publish each character in unicode '''
        for c in pstring:
            self.pub.publish(ord(c))  # Publish unicode for each character in string
        self.pub.publish(ord('\n'))   # Publish a newline to indicate end-of-string
        
    def run(self):
        ''' Input and publish until interrupt '''
        while not rospy.is_shutdown():  # Exit loop if Ctrl-C pressed
            pstring = input('Publish: ')
            self.pub_string(pstring)
    
if __name__=="__main__":

    myencoder = UniEncoder()
    myencoder.run()  

```
Now let's examine each section of the code in detail, starting with the `import` commands:
```python
import rospy
from std_msgs.msg import Int32
```
The library `rospy` is needed by all Python ROS code, and as you recall we included it as a dependency when we created the package above.  Our code will publish a `Int32` message type, and like other message types, we import it from `std_msgs.msg`.

```python
class UniEncoder:

    def __init__(self, topic_name='uni_char', node_name='uni_pub'):
        rospy.init_node(node_name) # Initialize ROS node
        # Create a publisher that publishes to topic 'uni_char'
        self.pub = rospy.Publisher(topic_name, Int32, queue_size=10)
        rospy.loginfo('Publishing to topic: ' + topic_name )
```
I recommend using classes to organize your code.  Here I have organized my node code in a class called `UniEncoder`. (The name you choose is not important to ROS).  The `__init__` function will set up our publisher.  I like to define key properties in its argument list, but this is optional.  The ROS node is created with: `rospy.init_node(<Node_name>)`.  Then `rospy.Publisher(<topic_name>, <message_type>, queue_size)` creates a publisher in our case with a topic named `uni_char`, a message type `Int32` and optionally a queue size.

The function `rospy.loginfo(<string>)` outputs 3 ways: to the terminal window, and to the node's log file, and to [rosout](http://wiki.ros.org/rosout).  The tool [rqt_console](http://wiki.ros.org/rqt_console) can view all the messages sent to [rosout](http://wiki.ros.org/rosout) from all your nodes; a convenient way to observe status when you have many nodes running.  Here it is nice to know that our publisher has been created.
```python
    def pub_string(self, pstring):
        ''' Input a string and publish each character in unicode '''
        for c in pstring:
            self.pub.publish(ord(c))  # Publish unicode for each character in string
        self.pub.publish(ord('\n'))   # Publish a newline to indicate end-of-string
```
Here is the function within our `UniEncoder` class that publishes each character of the input string as a separate message.  (This is for illustration only. There is a better way to publish strings, see Questions below.)
```python
    def run(self):
        ''' Input and publish until interrupt '''
        while not rospy.is_shutdown():  # Exit loop if Ctrl-C pressed
            pstring = input('Publish: ')
            self.pub_string(pstring)
```
Our `run` function contains the main loop.  It repeatedly inputs a string from the shell and calls the publish function.  The `rospy.is_shutdown()` checks for a shutdown initiated by an interrupt to quit the loop.
```python
if __name__=="__main__":

    myencoder = UniEncoder()
    myencoder.run()  
```
Finally, here is where code execution starts.  We simply create an instance of the `UniEncoder` class and call its `run` function.

## 4.2 The Subscriber

The full subscriber code is here:
```python
#!/usr/bin/env python
'''
    unicode_sub.py 

    This is a ROS node that subscribes to a topic containing Int32 elements
    Each integer is assumed to be unicode, and converted to a character and output on the terminal

    See also: unicode_pub.py

    Daniel Morris, Oct 2020
'''
import rospy
from std_msgs.msg import Int32

class UniDecoder:

    def __init__(self, topic_name='uni_char', node_name='uni_sub'):
        rospy.init_node(node_name) # Initialize ROS node
        # Create a subscriber that reads an Int32 from the topic and sends this to self.read_callback
        rospy.Subscriber(topic_name, Int32, self.read_callback)
        rospy.loginfo('Subscribing to topic: ' + topic_name)

    def read_callback(self, msg):
        ''' Decodes a single unicode character and outputs it to the terminal '''
        print(chr(msg.data),end='')

if __name__=="__main__":

    mydecoder = UniDecoder()  # Create a subscriber that reads and decodes the topic
    rospy.spin() # Wait and perform callbacks until node quits
```
Examining the code in detail, we can start with the `import` commands:
```python
import rospy
from std_msgs.msg import Int32
```
We have imported the same libraries as the publisher above.
```python
class UniDecoder:

    def __init__(self, topic_name='uni_char', node_name='uni_sub'):
        rospy.init_node(node_name) # Initialize ROS node
        # Create a subscriber that reads an Int32 from the topic and sends this to self.read_callback
        rospy.Subscriber(topic_name, Int32, self.read_callback)
        rospy.loginfo('Subscribing to topic: ' + topic_name)
```
Again I have organized the node using a class, this time called `UniDecoder` (but the name is not important).  Like previously we start by initializing a ROS node with `rospy.init_node(<node_name>)`.  It can be useful to log the fact that your node is subscribing to a topic using the command: `rospy.loginfo(<string>)`.  And then we tell ROS to create a subscriber with three arguments: `rospy.Subscriber(<topic_name>, <message_type>, <callback_function>)`.  The topic name and type should match those created by the publisher above.  The callback function is involked whenever a message is published to the topic, and we describe this next:
```python
    def read_callback(self, msg):
        ''' Decodes a single unicode character and outputs it to the terminal '''
        print(chr(msg.data),end='')
```
A subscriber callback expects one argument, the message.  (The `self` argument simply refers to the class that our callback function is part of.).  The field `msg.data` contains the data from the message.  In our case this is an `Int32` which can be converted to a character with `chr()`, and then printed to the screen.  This callback prints each subsequent character that it reads to the terminal. 
```python
if __name__=="__main__":

    mydecoder = UniDecoder()  # Create a subscriber that reads and decodes the topic
    rospy.spin() # Wait and perform callbacks until node quits
```
This is where execution or the Python code starts.  We initialize an instance of our `UniDecoder` class which creates the ROS node and initializes the subscriber along with the callback function.  Then we call `ros.spin()` which keeps the execution of the code here, and waits while handling the callbacks.  

## 4.3 Run the Package

You are ready to run the demo.  For now I assume all the following commands will be on the same host computer (or in the same container).  If you run nodes on different computers or containers you will need to set the `ROS_MASTER_URI` and `ROS_HOSTNAME` environment variables which we will describe later.  

You will need three shells for this demo.  In **each shell** source the overlay before any of the other commands.  This will add your workspace to the ROS path in the shall, and among other things, will enable tab completion.  Go ahead and create 3 shells and in each type:
````
source ~/catkin_ws/devel/setup.bash
````
In the first shell start the ROS master process.  In ROS 1 this must be done before any other ROS commands:
````
roscore
````
In a second shell let's run the publisher.  The format is: `rosrun <package_name> <node_name>`.  And as long as you have sourced the overlay, you can use tab completion (i.e. pressing the Tab key twice) to help complete the package name and node name: 
````
rosrun uni_demo unicode_pub.py
````
In a third shell let's run the subscriber:
````
rosrun uni_demo unicode_sub.py
````

To confirm that it is working, in the publisher shell type something and press Enter.  Then look at the output in the subscriber.  Here is my output for the 3 shells:
![roscore](.Images/roscore2.png)
![publisher](.Images/uni_pub.png)
![subscriber](.Images/uni_sub.png)

## 5 Questions

1. Notice the use of `rospy.spin()` in the subscriber.  
    - Why doesn't the publisher have a `rospy.spin()`?
    - What would happen if you removed `rospy.spin()` from the subscriber?  Try commenting it out and re-running the subscriber.
2. In the above example, the first line of text was sent successfully, but the second line has dropped characters.  Why?  Is this repeatable?  
3. What is the data type of the message?  How much data is sent in each published message?
4. If our goal were to just send strings, then it would be better to use a `std_msgs.msg.String` type.  Make a copy of the publisher and subscriber nodes in your package `src` folder and call the new code `string_pub.py` and `string_sub.py`.  Now modify the code so that they create and publish a `String` type topic.  Confirm that you are able to transfer strings without any loss of characters.  Among various changes, you will need to replace the import message line with:
```python
from std_msgs.msg import String
```
___
### [Back to Index](../Readme.md)
