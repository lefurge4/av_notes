# Coordinate Transforms

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)
## Contents

* [Coordinate Systems](#Coordinate-Systems)
* [Transforming Frames with Examples](#Transforming-Frames-with-Examples)
  * [`TransformFrames` Class](#TransformFrames-Class)
  * [Example of Transforming Frames](#Example-of-Transforming-Frames)
  * [Example of Transforming a `PoseArray`](#Example-of-Transforming-a-PoseArray)
  * [Example of Transforming Object Centroid Coordinates](#Example-of-Transforming-Object-Centroid-Coordinates)
* [Publishing Static Transforms](#Publishing-Static-Transforms)

___
# Coordinate Systems

Robotic platforms including autonomous vehicles are collections of components, each contributing in some way to the overall operation.  Now it is usually easiest to describe the operation of each component in its local coordinate system.  Consider, for example, a camera.  A camera projection model is simplest when the origin of the coordinate system is at the optical center and the z-axis is along the optical axis.  Thus in specifying a complex system like an AV, each component will have its own coordinate system for describing its properties. 

![Coordinates](.Images/Coordinates.png)

Coordinate systems can be illustrated as a 3-arrow axis like shown above and below.  These arrows refer to the `x`, `y` and `z` axes specified at the origin of the coordinates.  The coordinates are specified with a position of its origin relative to its parent (see arrow connecting each axis from its parent) and an orientation or rotation of the axis, again relative to its parent, see below:

![Coordinate Transforms](.Images/CoordinateTransforms.png)

ROS has tools for handling many coordinate systems and their relation to eachother.  Coordinate systems are typically specified in a tree structure with the pose of each coordinate system specified in relation to its parent coordinate system.  Using this tree structure it is straight forward to transform between any pair of coordinates.  ROS publishes a transform tree, `tf`, that relates the coordinate systems, and has subscriber and transformer tools that will provide the user with each conversion utilities.  

___
# Transforming Frames with Examples

While ROS has the tools needed, figuring out how to use them is not so easy.  Thus I have created a python class that provides an easy interface for accessing coordinate transforms.  The code is [av_notes/ROS/python/transform_frames.py](python/transform_frames.py), and this section will explain how it works and how to use it, as well as provide three examples using it.

## `TransformFrames` Class

The `TransformFrames` class is defined as follows:
```python
import rospy
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray, PoseStamped
from std_msgs.msg import Header
import tf2_ros, tf2_geometry_msgs

class TransformFrames():
    def __init__(self):
        ''' Create a buffer of transforms and update it with TransformListener '''
        self.tfBuffer = tf2_ros.Buffer()           # Creates a frame buffer
        tf2_ros.TransformListener(self.tfBuffer)   # TransformListener fills the buffer as background task
    
    def get_transform(self, source_frame, target_frame):
        ''' Lookup latest transform between source_frame and target_frame from the buffer '''
        try:
            trans = self.tfBuffer.lookup_transform(target_frame, source_frame, rospy.Time(0), rospy.Duration(0.2) )
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            rospy.logerr(f'Cannot find transformation from {source_frame} to {target_frame}')
            raise Exception(f'Cannot find transformation from {source_frame} to {target_frame}') from e
        return trans     # Type: TransformStamped

    def pose_transform(self, pose_array, target_frame='odom'):
        ''' pose_array: will be transformed to target_frame '''
        trans = self.get_transform( pose_array.header.frame_id, target_frame )
        new_header = Header(frame_id=target_frame, stamp=pose_array.header.stamp) 
        pose_array_transformed = PoseArray(header=new_header)
        for pose in pose_array.poses:
            pose_s = PoseStamped(pose=pose, header=pose_array.header)
            pose_t = tf2_geometry_msgs.do_transform_pose(pose_s, trans)
            pose_array_transformed.poses.append( pose_t.pose )
        return pose_array_transformed

    def get_frame_A_origin_frame_B(self, frame_A, frame_B ):
        ''' Returns the pose of the origin of frame_A in frame_B as a PoseStamped '''
        header = Header(frame_id=frame_A, stamp=rospy.Time(0))        
        origin_A = Pose(position=Point(0.,0.,0.), orientation=Quaternion(0.,0.,0.,1.))
        origin_A_stamped = PoseStamped( pose=origin_A, header=header )
        pose_frame_B = tf2_geometry_msgs.do_transform_pose(origin_A_stamped, self.get_transform(frame_A, frame_B))
        return pose_frame_B
```

The code depends on a number of packages including point and pose classes from `geometry_msgs`, the `Header` class from `std_msgs`, and two transform frame packages. The class `TransformFrames` has 4 functions, which we discribe next.  

The `__init__()` function allocates a buffer for storing transform frames, and then calls `TransformListener()` which will keep the buffer filled with the latest transform frames.  As a vehicle or robot moves, all of the coordinate transformations are updated in real time.

The `get_transform()` function looks up the latest transform from one frame to another frame from the frame buffer and returns this transform.  You'll see how to use this next.

Function `pose_transform()` transforms a `PoseArray` from its current coordinate system to a target coordinate system.  Let's look at the [definition](http://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/PoseArray.html) of a `PoseArray`:  
```
std_msgs/Header header
geometry_msgs/Pose[] poses
```
There are two important components: a `header` and a `poses` array.  A `Header` [definition](http://docs.ros.org/en/noetic/api/std_msgs/html/msg/Header.html) is:
```
uint32 seq
time stamp
string frame_id
```
The `stamp` component gives a time for `PoseArray` and `frame_id` is a text name of the frame (or coordinate system) in which the `PoseArray` is defined.  This is essential as in order to transform points we must first know which coordinate system they are defined in.  And as a side note, **headers** are a common feature of ROS messages and are helpful for interpreting the rest of the message.

The second component of a `PoseArray` is an array of `Pose` elements, where a `Pose` message is [defined](http://docs.ros.org/en/jade/api/geometry_msgs/html/msg/Pose.html) as:
```
geometry_msgs/Point position
geometry_msgs/Quaternion orientation
```
As one would expect, a pose consists of a position and rotation (specifying the orientation), and we see that ROS is using quaternions to encode rotations (although how they are encoded is not so important as they can be easily tranformed to another encoding such as a rotation matrix).  

If we have a set of positions along with orientations, or even just a set of positions defined in one coordinate system, we can pass them to `pose_transform()` as a `PoseArray` and this will transform them all to corresponding positions and orientations in another target coordinate system.  This is an easy way to make use of ROS's transform tree to switch between any two coordinate systems.

The last function, `get_frame_A_origin_frame_B()`, is a convenience function that will return the pose of one frame relative to another.  For example, if you want to know position and orientation of the origin of the camera coordinate frame in base coordinates, you can use this function.

___
## Example of Transforming Frames

Let's start up a Turtlebot on Gazebo, as explained in [Turtlebots_Gazebo](Turtlebots_Gazebo).  In one terminal do:
```
roslaunch turtlebot3_gazebo turtlebot3_empty_world.launch
```
And in another:
```
roslaunch turtlebot3_gazebo turtlebot3_gazebo_rviz.launch
```
Let's view the transform tree.  In another shell, run `rqt`, and under `Pluggins` / `Visualization`, select `TF Tree`.  Alternatively, type:
```
rosrun rqt_tf_tree rqt_tf_tree
```
This should bring up a view like this:

![TF Tree](.Images/tf_tree2.png)

We can see the coordinate systems being published in the transform tree.  Observe the `base_scan` frame has parent `base_link` which has parent `base_footprint`.  Now say we wish to know the location of the `base_scan` relative to the `base_footprint`.  From a shell you can obtain this with the command:
```
rosrun tf tf_echo <source_frame> <target_frame>
```
So in this case you would type:
```
rosrun tf tf_echo base_footprint base_scan
```
Try typing this, and see what you get when you reverse the order of the source and target frames. 

In your code you would import the above `TransformFrames` class and call the function `get_frame_A_origin_frame_B()`.  The following code in [av_notes/ROS/python/base_scan_coords.py](python/base_scan_coords.py) shows an example of how to do it:
```python
import rospy
from transform_frames import TransformFrames

if __name__=="__main__":

    rospy.init_node('base_scan_coords') 

    tf = TransformFrames()  # This initializes frame buffer
    rospy.sleep(1.0)         # Sleep so that buffer can fill up

    pose = tf.get_frame_A_origin_frame_B('base_scan','base_footprint')

    rospy.loginfo('Finding base_scan pose relative to base_footprint')
    rospy.loginfo(pose)
```
You can create a package with this code and `transform_frames.py` to try it out, or you can simpy run `base_scan_coords.py` directly as long as you source the underlay first.  Here is an example output:
```
(work) av:~$ rosrun base_scan base_scan_coords.py
[INFO] [1606156768.234353, 1036.141000]: Finding base_scan pose relative to base_footprint
[INFO] [1606156768.242499, 1036.141000]: header:
  seq: 0
  stamp:
    secs: 0
    nsecs:         0
  frame_id: "base_footprint"
pose:
  position:
    x: -0.032
    y: 0.0
    z: 0.182
  orientation:
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
```
From the output we see the following.  The frame `base_scan` is 0.182 meters above `base_footprint` in Z.  It is also offset by -0.032 m in X.  Finally, it's orientation is the same as `base_footprint`.  

Feel free to use `transform_frames.py` in your code.

## Example of Transforming a `PoseArray`

Let's use `transform_frames.py` to transform a `PoseArray`.  You can find a very simple example in [av_notes/ROS/python/base_scan_transform.py](python/base_scan_transform.py):
```python
import rospy
from transform_frames import TransformFrames
from geometry_msgs.msg import Pose, Point, Quaternion, PoseArray
from std_msgs.msg import Header

if __name__=="__main__":

    rospy.init_node('base_scan_coords') 

    tf = TransformFrames()  # This initializes frame buffer
    rospy.sleep(1.0)         # Sleep so that buffer can fill up

    # Here a pose defined in base_scan
    scan_pos = Pose(position=Point(2.,0.,0.), orientation=Quaternion(0.,0.,0.,1.))
    # Create a pose_array with frame_id='base_scan' to store it
    pose_array = PoseArray(header=Header(frame_id='base_scan',stamp=rospy.Time(0)))
    pose_array.poses.append(scan_pos)

    new_pose_array = tf.pose_transform(pose_array=pose_array, target_frame='base_footprint')

    rospy.loginfo('Original pose in base_scan')
    rospy.loginfo(pose_array)
    rospy.loginfo('New pose in base_footprint')
    rospy.loginfo(new_pose_array)
```
Here we define `scan_pose` with a position and orientation in `base_scan` coordinates.  We put it in a `PoseArray` and then call `pose_transform()` to transform it to `base_footprint` coordinates.  Then `scan_pose` is output in both `base_scan` coordinates and `base_footprint` coordinates.  Try running this code and compare the outputs.

## Example of Transforming Object Centroid Coordinates

Keep Gazebo running from the previous examples and add an object to the world like this:

![Gazebo Object](.Images/gazebo_cylinder.png)

Then you can use the code in [AV_Notes/ROS/python/centroid.py](python/centroid.py) to find the centroid of the lidar points on the object and publish the centroid, and plot it in Rviz like this:

![Gazebo Object](.Images/rviz_cylinder.png)

To get the large centroid blob, you will run the node as described below and add a PointCloud2 topic that subscribes to `/centroid`.

The code in `centroid.py` demonstrates how to publish the centroid in any frame you like, and ROS will plot it correctly.  To run the code, you can create a ROS package and copy in both `centroid.py` and `transform_frames.py`, or simply run it from its current folder.  if you create a package you will run it like this:
```
rosrun centroid centroid.py --frame base_scan
```
Or I could select a different frame:
```
rosrun centroid centroid.py --frame odom
```
Or why not in this frame:
```
rosrun centroid centroid.py --frame wheel_left_link
```
Try out some examples, and use teleop to move the robot around and look at the log output which tells you the centroid coordinates in your selected frame
```
roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
```
Teleop requires the *underlay* and *not the overlay* be sourced.

Have a look at the code below from `centroid.py` and make sure you can understand how the centroid coordinates are calculated in `base_scan` (relative to the Lidar center), and how they can be transformed into any other coordinates.  Then you publish the centroid in your selected coordinates, the important thing is that you specify which coordinates you are publishing it in.  So long as you do that, then RViz will transform it to its axis coordinates and plot it correctly.  

```python
#!/usr/bin/env python
import argparse
import rospy
from sensor_msgs.msg import PointCloud2, PointField
from sensor_msgs import point_cloud2
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Pose, Point, PoseArray
import numpy as np
from transform_frames import TransformFrames

class Centroid():
    def __init__(self, frame):
        self.pub_frame = frame
        rospy.init_node('centroid')
        self.trans = TransformFrames()
        self.pub = rospy.Publisher('/centroid', PointCloud2, queue_size=1)     
        rospy.Subscriber('/scan', LaserScan, self.lidar_callback)  
        rospy.spin()

    def lidar_callback(self, msg):               
        ranges = np.array(msg.ranges)           # Convert to Numpy array for vector operations
        angles = np.arange(len(ranges)) * msg.angle_increment + msg.angle_min # Angle of each ray
        good = ranges < np.inf                  # Only finite returns are good
        x = ranges[good] * np.cos(angles[good]) # vector arithmatic is much faster than iterating
        y = ranges[good] * np.sin(angles[good])
        raw_centroid = [[x.mean(),y.mean(),0.,0]]   # Centroid of object, in base_scan coordinates

        # Transform to a different coordinates, as specified by self.pub_frame:
        centroid, header = self.transform_points( raw_centroid, msg.header, self.pub_frame )

        # Output the frame we are describing the centroid in, and its coordinates:
        rospy.loginfo(f'frame: {header.frame_id}, centroid: {centroid[0][0]:.2f}, {centroid[0][1]:.2f}, {centroid[0][2]:.2f}')

        fields = [PointField('x', 0, PointField.FLOAT32,1),
                    PointField('y', 4, PointField.FLOAT32,1),
                    PointField('z', 8, PointField.FLOAT32,1),
                    PointField('intensity', 12, PointField.UINT32,1)]
        centroid_pc2 = point_cloud2.create_cloud(header, fields, centroid)

        self.pub.publish(centroid_pc2)

    def transform_points(self, pts, header, new_frame):
        ''' Transform point coordinates into new_frame '''
        # First see if we are actually changing frames:
        if header.frame_id == new_frame:
            return pts, header  # If not, then no need to transform anything

        # Convert points to PoseArray (Note: this is *not* efficient for many points, just a few)
        pa = PoseArray(header=header) # Header specifies which frame points start in
        for p in pts:
            pose = Pose(position=Point(p[0],p[1],p[2]))
            pa.poses.append(pose)
        # Call coordinate transform into new_frame:
        tran_pa = self.trans.pose_transform(pa, new_frame )
        # Convert PoseArray back into point list:
        new_pts = []
        for p in tran_pa.poses:
            new_pts.append( [p.position.x, p.position.y, p.position.z, 0] )  # For now ignoring intensity value

        return new_pts, tran_pa.header  # Return transformed points and new header
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Coordinates')
    parser.add_argument('--frame',    default='odom', type=str,    help='Centroid publish frame')
    args, unknown = parser.parse_known_args()  # For roslaunch compatibility
    if unknown: print('Unknown args:',unknown)

    Centroid(args.frame)
```


# Publishing Static Transforms

Some transforms do not change, for example the pose of a camera relative to robot.  We can publish these static transforms with a single command:
```
rosrun tf2_ros static_transform_publisher tx ty tz qx qy qz qw parent_frame child_frame
```
Here the position of the child frame relative to the parent frame is `tx,ty,tz`, and its orientation as a quaterion is `qx,qy,qz,qw`.   An example:
```
rosrun tf2_ros static_transform_publisher 0.2 .0 .2 0. 0. 0. 1. base_footprint camera_pose
```
This can be encoded in a launch file, see [launch/camera_pose.launch](#launch/camera_pose.launch).   Explanations of launch files are in [launch.md](#launch.md) to be covered later.  The launch file contains the above information:
````
<launch>
  <!-- This publishes a static transform of the camera extrinsics defining camera_pose relative to base_footprint
       The 7 parameters are: tx ty tz qx qy qz qw (translation and rotation quaterion)
       These are placeholder values -- make sure to replace these with the parameters you calculate from extrinsic calibration on your robot. 
       This launch file should go in the launch folder of your package. -->
  <node pkg="tf2_ros" name="Extrinsic_Publisher" type="static_transform_publisher" args="0.2 0. 0.2 0. 0. 0. 1. base_footprint camera_pose" >
  </node>
</launch>
````
To publish this type:
```
roslaunch <pkg_name> camera_pose.launch
```
Or simply
```
roslaunch <full_path_and_name_of_camera_pose.launch>
```



___
### [Back to Index](../Readme.md)






