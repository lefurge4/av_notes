# ROS Messaging and Topics

### Author: Daniel Morris, https://www.egr.msu.edu/~dmorris
## [Index](../Readme.md)
## Contents

* [ROS Messages](#ROS-Messages)
  * [ROS Topics](#ROS-Topics)
  * [ROS Services](#ROS-Services)
  * [ROS Actions](#ROS-Actions)
* [ROS Topics Details](#ROS-Topics-Details)
  * [Topics in a Rosbag](#Topics-in-a-Rosbag)
  * [Topics in the terminal: `rostopic`](#Topics-in-the-terminal-rostopic)
  * [Topics with: `rqt`](#Topics-with-rqt)
  * [Topics with: `rviz`](#Topics-with-rviz)
  * [Publishing and Subscribing to Topics](#Publishing-and-Subscribing-to-Topics)
  * [Order of Defining Subscribers and Publishers](#Order-of-Defining-Subscribers-and-Publishers)
  * [Subscribing to Images with OpenCV](#Subscribing-to-Images-with-OpenCV)
  * [Subscribing to Multiple Topics](#Subscribing-to-Multiple-Topics)
* [Defining Your Own Message Type](#Defining-Your-Own-Message-Type)

___
# ROS Messages 

Central to ROS is its messaging.  Messages are the glue that hold together the various processes running asynchronously on one or multiple CPUs.  These processes, or Nodes as they are called in ROS, can be written in any language as long as they communicate with a set of pre-defined messages and message protocols.  By specifying the messages of nodes and allowing them to run asynchronously, gives a very high degree of modularity to ROS.  It is straight forward to switch out one node for another node that may implement the agorithm differently with no need to recompile.  The result is that ROS is able to actualize a high degree of code reusability, that has not been practical in other languages.  

ROS messages are defined in simple text files.  The are defined recursively so that complex messages are built up by combining simpler messages.  ROS comes with a variety of pre-defined messages, and it is not hard to create new message types.  However, creating a new message type means that all the nodes that use it need to compiled with it, and this can be an impediment when integrating multiple nodes.  Thus, when possible it is preferable to use pre-defined message types even if is means some inefficiencies.  

Now there are three classes of messages, which will be described next.  The most important of these, ROS Topics, will be the focus of the rest of this document.  

## ROS Topics
The most widely used message type is a ROS Topic.  These operate on a publish and subscribe basis.  One or more nodes will register as publishing a topic, and then can publish to this topic.  Similarly one or more nodes can register as subscribers to a topic, and then they can read the topics.  With this model, a publishere does not know or care who reads the topic, and the subscriber does not know who is publishing the topic.  

Messages can be sent at unknown time intervals, and a subscriber may miss a message.  There is a buffering mechanism to reduce dropped messages, but no guarantee that a message will be read.  Implementations should take this into account.  Topics are good for high-bandwidth data, such as sensor data from a camera, lidar, imu etc.  For these sensors, if a frame is missed it is usually best to go onto the next frame, rather than retrieve the missed one.

Topics are defined in `.msg` files, and it is standard to store them in the `<package_name>/msg` folder.

Some commonly used messages are `geometry_msgs` and `std_msgs`.  To use message `Point` which is part of `geometry_msgs` your python code would have the following import:
```python
from geometry_msgs.msg import Point
```
To find out what is in the message, you can look online.  In this case documentation is at [https://docs.ros.org/en/api/geometry_msgs/html/msg/Point.html](https://docs.ros.org/en/api/geometry_msgs/html/msg/Point.html).  We see that the text file defining this type is simply:
````
# This contains the position of a point in free space
float64 x
float64 y
float64 z
````
The `#` indicates a comment line.  This same information can be found from the command line using `rosmsg` as follows:
````
~$ rosmsg info Point
[geometry_msgs/Point]:
float64 x
float64 y
float64 z
````

Now messages are defined recursively.  For example, a `Pose` message is defined by the text file
````
# A representation of pose in free space, composed of position and orientation. 
Point position
Quaternion orientation
````
Here a `Point` is defined as above.  Similarly a `Quaternion` is another message type that you can look up.  To discover all the topics being published or subscribed to, use:
````
rostopic list
````
## ROS Services
With a topic, a sensor does not know if anyone received its message.  This is addressed by `services` which define bidirectional communication.  A client sends a request and a service sends a response.  When the client receives the return message, it knows that the its message has been read.  

Services are intended to be fast, and service calls are blocking.  That is, the client will wait until it gets a response from the service before continuing its operations.  

There may be some delay in getting a response, so code should be tolerant of small delays.  However, if there could be long delays, then it may be better to use an `action` service, see below.

Services are defined in `.srv` files and it is standard to store them in the `<package_name>/srv` folder.  An example service message file could be:
````
# request
int8 wheel_number
---
# response
float32 pressure
float32 temperature
````
The format specifies the client request message first, then a `---` on a single line, followed by the service response message. Here the client is sends a request and specifies a `wheel_number`, and the service returns `pressure` and `temperature`.  Any topic message can be included in a service message.

## ROS Actions

If a request could take a long time to complete, then an `action` may be a better message type.  Actions are non-blocking, bi-directional messages with timeouts.  They also include preemption and cancelling.  

Actions are stored as a `.action` file in the `<package_name>/action` folder.  The format is as follows:
````
# Goal
Pose target_pose
---
# Result
Pose final_pose
---
# Feedback
Pose current_pose
````
The three components are `goal`, `result` and `feedback`, separated by `---` lines.  In this simple example the client requests a target pose, (see geometry_msgs/msgs/Pose.msg), and when done the action server returns its final pose.  In the mean time, the action server can provide feedback, which in this case is the current pose.  

We will be using action servers in our State Machines.

___
# ROS Topics Details

The rest of this document focuses on the basic concept of a ROS Topic.  Each topic has a name and a fixed message type.  Nodes can publish to one or more topics and/or subscribe to one or more topics.  For example, a camera node may read images from a camera and publish these to a `camera` topic.  Another node such as an vehicle detector node, might subscribe to this topic.  In doing so, it will be fed images as they are published, it processes them and then publishes its detections on a different topic, perhaps called `vehicles`.  Meanwhile a path planner could subscribe to the `vehicles` topic and `map` topic and publish to a `path` topic.

The operation of a vehicle, what it sees and what it does, is characterized by the topics published by its nodes.  Thus it is very useful to view, plot, record and examine topics.  ROS provides a number of tools for topic exploration, and this document is an introduction to these.


## Topics in a Rosbag

ROS topics can be easily recorded and stored in a `rosbag`.   A `rosbag` is created by a special node listening to all or a subset of topics on the ROS bus, and writing those to disk as it receives them.  It is straight forward to read from a bag and re-publish the topics on the ROS bus.   So, for example, vehicle sensor data are published to the ROS bus, and these can be recorded in a rosbag.  Then at a later point this bag can be replayed and the vehicle's run essentially regenerated.  

Let's explore a rosbag that was recorded on the MSU CANVAS vehicle:  

![CANVAS Vehicle](.Images/CANVAS_vehicle.jpg)

Download the rosbag `msu_sv_short.bag` from [https://drive.google.com/drive/folders/1Tx0CzG8srHAS2AYjCCD9ftkC6SUtlfRp?usp=sharing](https://drive.google.com/drive/folders/1Tx0CzG8srHAS2AYjCCD9ftkC6SUtlfRp?usp=sharing) and put in your data folder.  

Ensure you have `roscore` running in one terminal.

In another terminal type:
````
rosbag play --loop --clock <path_to_bag.bag>
````
You should see something like the following:
```bash
[ INFO] [1602167251.920218000]: Opening data/msu_sv_short.bag

Waiting 0.2 seconds after advertising topics... done.

Hit space to toggle paused, or 's' to step.
 [RUNNING]  Bag Time: 1545330105.505899   Duration: 16.653518 / 224.114127
```

If you see this, that means that the rosbag is being played back and its topics published to the ROS bus, and so are available to any ROS applications.   Those are not visible in the terminal, so how can you see what is available? 

## Topics in the terminal: `rostopic`

ROS has a large number of utilities for analyzing and visualizing messages.  Within a terminal, `rostopic` provides a summary of topics being published, and can be used to query details about each topic.  First, to obtain a list of published topics use:
```bash
rostopic list -v
```

The `info` option will provide more information about a given topic, for example the `/points_raw` topic containing the Lidar point cloud:
```bash
rostopic info /points_raw
```
This tells us that this topic uses the message type: `sensor_msgs/PointCloud2`.

You can see how frequently data are sent on this topic using the 'hz' option:
```bash
rostopic hz /points_raw
```
Press *Ctlr+C* to exit.  To see all the data being published live, use the `echo` option:
```bash
rostopic echo /points_raw
```
Once again, use *Ctrl+C* to quit.  When you want to find all the all the published topics, use tab completion: 
````
rostopic echo /<tab>-<tab>
````
If you start the name and press the tab key twice, ROS will complete it.

## Topics with: `rqt`

Next, let's use the ROS console called `rqt` to examine the topics, which has [online documentation here](http://wiki.ros.org/rqt).  In another terminal type:
````
rqt
````
![rqt](.Images/rqtEmpty.png)

This opens an `rqt` window.  There are many plugins to explore.  For now, select the Topic Monitor from the menu:

![Topic Monitor](.Images/rqtMenu.png)

In this view you can select which ever topics you are interested and get further information about them including their published frequency and message type and sample data in them.  Take some time to explore the various topics and their components.

![PointsRaw Topic](.Images/rqtPoints.png)

Next, let's explore the video being published from this rosbag.  First close the Topic Monitor panel in rqt using the right-most `X` in its panel.  Then open the Image View plugin:

![Image Viewer](.Images/rqtImageView.png)

You will need to select the `/camera/camera_raw` topic, at which point you should see the video displaying in a panel.  

![Image Viewer](.Images/rqtImageView2.png)

A very simple image viewing package is called `image_view`, which just plots the image:
````
rosrun image_view image_view image:=/camera/image_raw
````

An alternative to using the `rqt` console, is to directly call the visualization utility you need.  Here is a partial list of rqt utilities:

````
rqt_image_view - Displays an image from a sensor_msgs/Image topic
rqt_plot       - Allows plotting of numeric data
rqt_tf_tree    - Shows the parent/child relationships for reference frames
rqt_graph      - Shows how nodes and topics are connected to each other
rqt_publisher  - Allows you to manually publish topics 
rqt_console    - Shows debug, info and error messages published by ROS nodes
rqt_gui        - Allows user to run multiple rqt plugins in a single window
````

These tools can be run directly, such as by typing `rqt_plot`, or called with the standard command: `rosrun <package_name> <node_name>`.  For example:
````
rosrun rqt_plot rqt_plot
````
You will see a blank plot come up. To add data, simply type the topic name and path to numeric data. For example, to plot the vehicle's forward speed, use the topic
`/vehicle/twist/twist/linear/x` You can adjust the x and y axes by clicking the green checkbox and adjusting the minimum and maximum values.

![rqt_plot](.Images/rqtPlot.png)


## Topics with: `rviz`

The most extensive visualization tool ROS has to offer is RViz. RViz provides both 3D and 2D visualization.  Before running RViz, ensure it uses the rosbag time, rather than current time, with this command:

```
rosparam set /use_sim_time true
```
Failing to do this may require you to constantly reset the time in the bottom left button in order to see the data.  Then launch RViz with either `rosrun rviz rviz`, or simply:
```bash
rviz
```
*Note:* Under WSL I got a segfault when first running `rviz`, possibly due to an incorrect OpenGL version.  To correct it I set the below in my `~/.bashrc` file, and then `rviz` runs fine:
````
LIBGL_ALWAYS_INDIRECT=0
````
![Segmentation fault](.Images/rvizSegfault.png)

If you get a sequence of warnings that look like the following, don't worry.  This is just detecting a jump backwards in time of 8 seconds when the `rosbag play` reaches the end of the rosbag and loops back to the start.
````
[ WARN] [1602210357.133319400, 1545330169.000570460]: Detected jump back in time of 7.98864s. Clearing TF buffer.
````

At first, RViz does not show any information, because no topics have been added yet. Let's first add a visualization of the transformation frames. 
Click `Add`, then scroll down to `TF`, then click `OK`.

![RViz](.Images/rviz2.png)

At first, it looks like nothing has happened. However, take a look in the upper left hand corner: notice how RViz is using the `map` frame as its fixed reference frame.
Now, take a look in the upper right-hand corner: the `Target Frame` is set to `Fixed frame`, with coordinates 0,0,0.  This `Fixed frame` is at the origin of the map, and not necessarily near the vehicle or robot.  Now there is a series of transformations between the `map` frame and the `base_link` frame of the vehicle (which is typically located under the center rear axle).  These transformation data are included in the `tf-tree` which defines a number of references frames and their relation to each other.  Available reference frames are in the dropdown to the right of `Target Frame`.

Change the camera focus to the vehicle's base link by clicking the dropdown on the right next to `Target Frame`, select `base_link`, then click the `Zero` button. 
You should see a depiction of the reference frames from the system:

![RViz base_link](.Images/rviz3.png)

*Note:* in WSL the right panel often does not update when changed, and even when I select `base_link`, it continues to display `<Fixed frame>`.  By clicking twice on the arrow in the green-circle, you close and open the panel, and so update it and see the `base_link` selection.  In addition, the coordinate system size of `1` is a bit too small.  Here I updated the coordinate scale to 6, see purple rectangle on the left panel.  

Now, add the lidar data by clicking `Add`, then click the `By Topic` tab, then click on the `/points_raw` topic, then click the `PointCloud2` entry under the
`/points_raw` dropdown, then click `OK`. You should see lidar points show up:

![RViz Points](.Images/rviz4.png)

*Note:* To make the points easier to see I selected `Points` in the Style option.  If you are not seeing any points, make sure that the `Target Frame` is set to `base_link`, and press the `Zero` as above.  This centers the view at the vehicle coordinate rather than possibly at the map origin.

## Publishing and Subscribing to Topics

We have already seen examples of publishing and subscribing to topics within a ROS node in [ROS_Start](ROS_Start.md).  Here is code minimal node that publishes strings to a topic called `greetings`:
```python
#!/usr/bin/env python
import rospy
from std_msgs.msg import String

if __name__=="__main__":
    rospy.init_node('Publish_String')
    rospy.sleep(2.)  # Wait for node to initialize
    rate = rospy.Rate(1) # publish at 1 Hz
    pub = rospy.Publisher('greetings', String, queue_size=10)
    for s in ['Good','morning.','How','are','you','today?']:
        pub.publish(s)
        rate.sleep()
```
The command `rospy.Publisher(<topic_name>,<message_type>,...)` creates a publisher and requires a topic name and message type.  Then calling `publish()` with this publisher will publish a message.

Subscribers read published topics.  Since a subscriber cannot know when a topic will be published, rather than continually polling for a topic, the most convenient way to implement a subscriber is with a callback function that is called whenever a message appears on the selected topic.  A simple example of this is that reads the `greetings` topic is as follows:
```python
#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def read_callback( msg ):
    print(msg.data) # Print the string each time a message is received

if __name__=="__main__":
    rospy.init_node('Subscriber')
    rospy.Subscriber('greetings', String, read_callback)
    rospy.spin()
```
Here the command `rospy.Subscriber(<topic_name>,<message_type>,<callback>)` specifies a callback function that will be called whenever a topic is detected.  The callback is called with the message as its argument, as can be seen above.  The `data` field of the message contains the data that was published.  

## Order of Defining Subscribers and Publishers

Be cautious when to define a subscriber in your `init` function.  As soon as it is defined, it can be called.  Make sure that all components of the function have been defined.  It is quite common to have a publisher inside the callback function for the subscriber.  Make sure this publisher has been defined first, before the subscriber is defined, to avoid calling it before it is defined.

## Subscribing to Images with OpenCV
Subscribing to an image is not much different than subscribing to another topic.  There are two main ROS image types: uncompressed and compressed, and you'll need to import the appropriate message type depending on which you use.  Additionally, you will need an OpenCV bridge to convert to an OpenCV array.  The following example can subscribee to either an uncompressed or a compressed image topic.
```python
#!/usr/bin/env python
''' Example subscribing to both compressed and uncompressed image topics '''
import rospy
import cv2
from sensor_msgs.msg import Image, CompressedImage
from cv_bridge import CvBridge, CvBridgeError

class img_sub:
    def __init__(self, do_compressed):
        self.do_compressed = do_compressed
        self.bridge = CvBridge()
        if self.do_compressed:
            rospy.Subscriber('compressed_topic', CompressedImage, self.image_cb)
        else:
            rospy.Subscriber('uncompressed_topic', Image, self.image_cb)

    def image_cb( self, msg ):
        try:
            if self.do_compressed:
                img = self.bridge.compressed_imgmsg_to_cv2(msg,'bgr8')
            else:
                img = self.bridge.imgmsg_to_cv2(msg,'bgr8')
        except CvBridgeError as e:
            print(e)
        cv2.imshow('Image',img )
        if cv2.waitKey(1) & 0xFF == ord('q'):
            rospy.signal_shutdown('Quitting')

if __name__=="__main__":
    rospy.init_node('Image Subscriber')
    image_sub(do_compressed=True)
    rospy.spin()
```

## Subscribing to Multiple Topics

In some cases it is necessary for a single node to subscribe to two or more topics and process corresponding messages from each topic.  The subscriber shown above cannot do this as it is called by and fed a single topic. 

However, there is a ROS package that handles subscribing to multiple topics and synchronizing them.  This is the `message_filters` package, and documentation is available here: [http://wiki.ros.org/message_filters](http://wiki.ros.org/message_filters).  The following is an example, see [av_notes/ROS/python/topic_sync.py](python/topic_sync.py), of how a callback can be fed two messages that are time-synchronized.  Note: the messages should both have header of type: `std_msgs/Header` which gives the time of the message.  This is used to find matching messages between the topics.
```python
#!/usr/bin/env python
import cv2
import rospy
import message_filters
from sensor_msgs.msg import CompressedImage, CameraInfo
from cv_bridge import CvBridge

class CircleDrive:
    
    def __init__(self):
        self.bridge = CvBridge()
        img_sub = message_filters.Subscriber('raspicam_node/image/compressed', CompressedImage)
        info_sub = message_filters.Subscriber('raspicam_node/camera_info', CameraInfo)
        self.ts = message_filters.TimeSynchronizer([img_sub, info_sub], 2)
        self.ts.registerCallback(self.show_image)        

    def show_image(self, img_msg, info_msg):
        img = self.bridge.compressed_imgmsg_to_cv2(img_msg,'bgr8')
        focal_x = info_msg.K[0]  # First element of K is x focal length
        cv2.putText(img, 'Fx: '+str(focal_x), (5,30), cv2.FONT_HERSHEY_SIMPLEX, 1, (200,0,200), 2)
        cv2.imshow("Image",img)
        if (cv2.waitKey(2) & 0xFF) == ord('q'):
            rospy.signal_shutdown('Quitting')

if __name__=="__main__":

    rospy.init_node('show_focal', anonymous=True)
    CircleDrive()
    rospy.spin()
```
Notice that in the `__init__` function two subscribers are defined using `message_filters` without callbacks.  Then a `TimeSynchronizer` function is used to combine these messages.  This time-based synchronization assumes each message contains a standard header (see `Header` in `std_msgs.msg`) which carries a time stamp.  By comparing time-stamps the `TimeSynchronizer` can match messages from different topics.  That means if you want messages to be grouped in this way you need to ensure their headers have the same time-stamp.  Finally, a callback is registered for the combined message.  As you see it has two arguments: namely both messages.  This example shows two messages from two topics, but any number of messages can be combined this way.

To try this out, play a rosbag collected from a Turtlebot in one terminal, and in another terminal type:
```
python topic_sync.py
```
You should get an output that looks like this, where the current camera focal length is written on the image.  (Don't forget to source the overlay.)  We have subscribed to both an image topic and the camera info topic in a single callback, which is demonstrated by plotting the x focal length on the image.  In this case the camera info is not changing over time, but nevertheless is published at the same rate as the images and with corresponding headers and time stamps which enables time synchronization.

![Sync Topics](.Images/sync_topics.png)

If the ROS topics being synchronized do not have exactly the same time in their time-stamps, then use the `ApproximateTimeSynchronizer` function.  Note that I have found this can be finicky when playing a rosbag in a loop (it doesn't handle time-resetting well and can get stuck), so I recommend using `TimeSynchronizer` whenever possible for more reliable performance.

___
# Defining Your Own Message Type

ROS comes with a host of pre-defined message types (topics, services and actions).  When possible it is preferable to use one of these types, rather than create your own, as this makes it easier to plug-and-play with other packages.   I recommend using standard message types even if it entails some inefficiencies such as only partially filling the message blocks.  Nevertheless, it is not hard to define and use your own message types.  

There is online documentation available for creating a new message type and using it.  The key documents are: 

* [http://wiki.ros.org/msg](#http://wiki.ros.org/msg): How to create message types
* [http://wiki.ros.org/ROS/Tutorials/CreatingMsgAndSrv](#http://wiki.ros.org/ROS/Tutorials/CreatingMsgAndSrv): A tutorial on using a message and/or service in your code

The following is a summary of what is involved in creating a message as described in [http://wiki.ros.org/msg](#http://wiki.ros.org/msg).  A message is defined within a ROS package.  For ease of explanation, the following steps will define a message called `my_message` in package `my_package`:

1. Within the `my_package` folder, create a `msg` folder and add to it your `my_message.msg` file that defines your message type (see above for examples).
2. Uncomment the following two lines in the `package.xml` file to enable build and run dependence on messages, [see here](http://wiki.ros.org/msg#Building_.msg_Files). 
```
  <build_depend>message_generation</build_depend>
  <exec_depend>message_runtime</exec_depend>
```
3. Make a series of adjustments to the `CMakeLists.txt` file, [see here for details](http://wiki.ros.org/msg#Building_.msg_Files).  This will add your new message type, and any messages it depends on, to the build procedure.
4. Use `rosmsg show my_message` to confirm that your new message is properly configured.  If there is an error, then fix it.
5. Run `catkin_make` in your `~/catkin_ws` folder to build your new message and to make it available to nodes.  If there are errors, you may need to adjust the dependencies in the `CMakeLists.txt` file.

Once your message compiles, then any code in your workspace can include it.  Simply add the following line:
```
from my_package.msg import my_message
```
In lab 4 you will create your own message type.

___
### [Back to Index](../Readme.md)






